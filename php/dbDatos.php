<?php
  include("conexion.php");
  header("Content-Type: text/html;charset=utf-8");

  switch ($_REQUEST['action']){
    case 'getPatg':
      getPatg();
      break;
    case 'getPovi':
      getPovi();
      break;
    case 'getIdat':
      getIdat();
      break;
  }
  function getPatg(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $qry = 'SELECT * FROM accionesgrupales WHERE id_formato='.$id_formato;
    $qry = mysqli_query ($con,$qry);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
          "id_formato" => intval($rows['id_formato']),
          "formato" => $rows['formato'],
          "codigo" => $rows['codigo'],
          "revision" => intval($rows['revision']),
          "fechaExpedicion" => $rows['fechaExpedicion'],
          "id_tutor" => intval($rows['id_tutor']),
          "id_carrera" => intval($rows['id_carrera']),
          "id_grupo" => intval($rows['id_grupo']),
          "cuatrimestre" => $rows['cuatrimestre'],
          "horasParcial" => intval($rows['horasParcial']),
          "horasSemana" => intval($rows['horasSemana']),
          "estatus" => intval($rows['estatus']),
          "objetivo" => $rows['objetivo'],
          "director" => $rows['director']
        );
    }
    print_r(json_encode($array[0]));
  };
  function getPovi(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $qry = 'SELECT * FROM programavisitas WHERE id_formato='.$id_formato;
    $qry = mysqli_query ($con,$qry);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
          "id_formato" => intval($rows['id_formato']),
          "formato" => $rows['formato'],
          "codigo" => $rows['codigo'],
          "revision" => intval($rows['revision']),
          "fechaExpedicion" => $rows['fechaExpedicion'],
          "id_tutor" => intval($rows['id_tutor']),
          "id_carrera" => intval($rows['id_carrera']),
          "id_grupo" => intval($rows['id_grupo']),
          "numAlumnos" => intval($rows['numAlumnos']),
          "estatus" => intval($rows['estatus']),
          "director" => $rows['director']
        );
    }
    print_r(json_encode($array[0]));
  };
  function getIdat(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $qry = 'SELECT * FROM autoevaluaciones WHERE id_formato='.$id_formato;
    $qry = mysqli_query ($con,$qry);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
          "id_formato" => intval($rows['id_formato']),
          "formato" => $rows['formato'],
          "codigo" => $rows['codigo'],
          "revision" => intval($rows['revision']),
          "fechaExpedicion" => $rows['fechaExpedicion'],
          "id_tutor" => intval($rows['id_tutor']),
          "estatus" => intval($rows['estatus']),
          "director" => $rows['director']
        );
    }
    print_r(json_encode($array[0]));
  };
?>
