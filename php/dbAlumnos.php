<?php
  include("conexion.php");
  header("Content-Type: text/html;charset=utf-8");

  switch ($_REQUEST['action']){
    case 'getREIN':
      getREIN();
      break;
    case 'setREIN':
      setREIN();
      break;
    case 'getPreguntas':
      getPreguntas();
      break;
  }
  function getREIN(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id = intval($data->id);
    $qry = mysqli_query ($con,'SELECT * from reportesindividuales WHERE id_alumno='.$id);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "revision" => intval($rows['revision']),
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "estatus" => intval($rows['estatus']),
            "comentarios" => $rows['comentarios'],
            "director" => $rows['director'],
        );
    }
    if($array)
    print_r(json_encode($array[0]));
  };
  function setREIN(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $formato = $data->formato->formato;
    $codigo = $data->formato->codigo;
    $revision = intval($data->formato->revision);
    $fechaExpedicion = $data->formato->fechaExpedicion;
    $id_tutor = intval($data->formato->tutor->id_usuario);
    $id_carrera = intval($data->formato->tutor->carrera->id_carrera);
    $id_grupo = intval($data->formato->tutor->grupo->id_grupo);
    $director = $data->formato->director;
    $preguntas = $data->formato->preguntas;
    $id_alumno = intval($data->formato->alumno->id_usuario);
    $edad_alumno = intval($data->formato->alumno->edad);
    $lugar_alumno = $data->formato->alumno->lugarNacimiento;
    $telefono_alumno = $data->formato->alumno->telefono;
    $vive_alumno = $data->formato->alumno->vive;
    $estado_alumno = $data->formato->alumno->estado;
    $residencia_alumno = $data->formato->alumno->lugarResidencia;
    $municipio_alumno = $data->formato->alumno->municipio;
    $nivel_alumno = $data->formato->alumno->nivel;
    $cuatrimestre_alumno = $data->formato->alumno->cuatrimestre;
    if($id_formato>0){
      $qry = 'UPDATE reportesindividuales SET id_tutor='.$id_tutor.',estatus=1
                                  WHERE id_formato='.$id_formato;
    }else{
      $qry = 'INSERT INTO reportesindividuales (formato,codigo,revision,fechaExpedicion,id_tutor,id_carrera,id_grupo,director,id_alumno) VALUES
              ("'.$formato.'","'.$codigo.'",'.$revision.',"'.$fechaExpedicion.'",'.$id_tutor.','.$id_carrera.','.$id_grupo.',"'.$director.'",'.$id_alumno.')';
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      if($id_formato>0){
        //hacer algo si es que se estan editando, puede ser que se agreguen mas, quede igual o se eliminen algunas
      }else{
        $last_id = $con->insert_id;
        $numero=0;
        foreach ($preguntas as $key => $value) {
          $qry = 'INSERT INTO preguntasreporteindividual (id_formato,numero,respuesta)  VALUES ('.$last_id.','.$numero.',"'.$value->respuesta.'")';
          $qry_res = mysqli_query($con,$qry);
          if($qry_res){
            $numero++;
          }
        }
        $qry = 'UPDATE usuarios SET telefono="'.$telefono_alumno.'",edad='.$edad_alumno.',nivel="'.$nivel_alumno.'",lugarNacimiento="'.$lugar_alumno.'",
                estado="'.$estado_alumno.'",lugarResidencia="'.$residencia_alumno.'",municipio="'.$municipio_alumno.'",
                cuatrimestre="'.$cuatrimestre_alumno.'",vive="'.$vive_alumno.'" WHERE id_usuario='.$id_alumno.'';
        $qry_res = mysqli_query($con,$qry);
        if($qry_res){
          $arr = array('success' => true,'error->alumno' => false);
        }else{
          $arr = array('success' => false,'error->preguntas' => true);
        }
      }
    }
    else{
      $arr = array('success' => false,'error->formato' => true);
    }
    print_r(json_encode($arr));
  };
  function getPreguntas(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->rein->id_formato);
    $qry = mysqli_query ($con,'SELECT * from preguntasreporteindividual WHERE id_formato='.$id_formato);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "numero" => intval($rows['numero']),
            "respuesta" => $rows['respuesta']
        );
    }
    print_r(json_encode($array));
  };
?>
