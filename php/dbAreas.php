<?php
  include("conexion.php");
  header("Content-Type: text/html;charset=utf-8");
  switch ($_REQUEST['action']){
    case 'getAreas':
      getAreas();
      break;
    case 'setArea':
      setArea();
      break;
    case 'dropArea':
      dropArea();
      break;
  }
  function getAreas(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from areas where activo=1');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
          "id_area" => intval($rows['id_area']),
          "area" => $rows['area'],
          "id_carrera" => intval($rows['id_carrera']),
        );
    }
    print_r(json_encode($array));
  };
  function setArea(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_area = intval($data->area->id_area);
    $area = $data->area->area;
    $id_carrera = intval($data->area->carrera->id_carrera);
    if($id_area>0){
      $qry = 'UPDATE areas SET area="'.$area.'",id_carrera='.$id_carrera.' WHERE id_area='.$id_area;
    }else{
      $qry = 'INSERT INTO areas (area,id_carrera) VALUES ("'.$area.'",'.$id_carrera.')';
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      $last_id = $con->insert_id;
      $arr = array('success' => $last_id, 'error' => false);
    }
    else{$arr = array('success' => false,'error' => true);}
    print_r(json_encode($arr));
  };
  function dropArea(){
      global $con;
      $data = json_decode(file_get_contents("php://input"));
      $idU = intval($data->id_area);
      $qry_res = mysqli_query($con,'UPDATE areas SET activo=0 WHERE id_area='.$idU);
      if($qry_res){$arr = array('success' => true, 'error' => false);}
      else{$arr = array('success' => false,'error' => true);}
      print_r(json_encode($arr));
  };
?>
