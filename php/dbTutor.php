<?php
  include("conexion.php");
  header("Content-Type: text/html;charset=utf-8");

  switch ($_REQUEST['action']){
    case 'getDirector':
      getDirector();
      break;
    case 'getCoordinador':
      getCoordinador();
      break;
    case 'getAlumnos':
      getAlumnos();
      break;
    case 'getPATG':
      getPATG();
      break;
    case 'setPATG':
      setPATG();
      break;
    case 'getActividades':
      getActividades();
      break;
    case 'getPOVI':
      getPOVI();
      break;
    case 'setPOVI':
      setPOVI();
      break;
    case 'getVisitas':
      getVisitas();
      break;
    case 'getIDAT':
      getIDAT();
      break;
    case 'setIDAT':
      setIDAT();
      break;
    case 'getPreguntas':
      getPreguntas();
      break;
  }
  function getDirector(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->usuario->id_usuario);
    $qry = mysqli_query ($con,'SELECT us.nombreCompleto from usuarios as us
      join usuarios_carreras as uca ON us.tipo=3 and us.id_usuario=uca.id_usuario and uca.id_carrera in (select id_carrera from usuarios_carreras where id_usuario='.$id_usuario.') GROUP by us.id_usuario');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "nombreCompleto" => $rows['nombreCompleto']
        );
    }
    if($array)
    print_r(json_encode($array[0]));
  };
  function getCoordinador(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from usuarios WHERE tipo=2');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "nombreCompleto" => $rows['nombreCompleto']
        );
    }
    if($array)
    print_r(json_encode($array[0]));
  };
  function getAlumnos(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_tutor = $data->tutor->id_usuario;
    // $id_grupo = $data->tutor->id_grupo;
    $qry = mysqli_query ($con,'SELECT * from usuarios AS usu JOIN usuarios_grupos AS aug
                                ON usu.id_usuario=aug.id_usuario AND usu.tipo=6  AND aug.id_tutor='.$id_tutor.'');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "nombreCompleto" => $rows['nombreCompleto'],
            "nombre" => $rows['nombre'],
            "appat" => $rows['appat'],
            "apmat" => $rows['apmat'],
            "id_usuario" => intval($rows['id_usuario']),
            "matricula" => $rows['matricula'],
            "id_grupo" => intval($rows['id_grupo']),
            "id_carrera" => intval($rows['id_carrera']),
            "telefono" => $rows['telefono'],
            "correo" => $rows['correo'],
            "curp" => $rows['curp'],
            "nss" => $rows['nss'],
            "edad" => intval($rows['edad']),
            "lugarNacimiento" => $rows['lugarNacimiento'],
            "municipio" => $rows['municipio'],
            "estado" => $rows['estado'],
            "lugarResidencia" => $rows['lugarResidencia'],
            "cuatrimestre" => $rows['cuatrimestre'],
            "vive" => $rows['vive'],
            "nivel" => $rows['nivel']
        );
    }
    if($array)
    print_r(json_encode($array));
  };
  /**
   	 * Block comment
   	 *
   	 * @param type
   	 * @return void
  ██████   █████  ████████  ██████
  ██   ██ ██   ██    ██    ██
  ██████  ███████    ██    ██   ███
  ██      ██   ██    ██    ██    ██
  ██      ██   ██    ██     ██████
  */

  function getPATG(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->usuario->id_usuario);
    $qry = mysqli_query ($con,'SELECT * from accionesgrupales WHERE id_tutor='.$id_usuario);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "revision" => intval($rows['revision']),
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "cuatrimestre" => $rows['cuatrimestre'],
            "horasParcial" => intval($rows['horasParcial']),
            "horasSemana" => intval($rows['horasSemana']),
            "estatus" => intval($rows['estatus']),
            "objetivo" => $rows['objetivo'],
            "comentarios" => $rows['comentarios']
        );
    }
    if($array)
    print_r(json_encode($array[0]));
  };
  function setPATG(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $formato = $data->formato->formato;
    $codigo = $data->formato->codigo;
    $revision = intval($data->formato->revision);
    $fechaExpedicion = $data->formato->fechaExpedicion;
    $id_tutor = intval($data->formato->tutor->id_usuario);
    $id_carrera = intval($data->formato->tutor->carrera->id_carrera);
    $id_grupo = intval($data->formato->tutor->grupo->id_grupo);
    $cuatrimestre = $data->formato->cuatrimestre;
    $horasParcial = intval($data->formato->horasParcial);
    $horasSemana = intval($data->formato->horasSemana);
    $objetivo = $data->formato->objetivo;
    $actividades = $data->formato->actividades;
    $director = $data->formato->director;

    if($id_formato>0){
      $qry = 'UPDATE accionesgrupales SET id_tutor='.$id_tutor.',id_carrera='.$id_carrera.',id_grupo='.$id_grupo.',
                                  cuatrimestre="'.$cuatrimestre.'",horasParcial='.$horasParcial.',
                                  horasSemana='.$horasSemana.',objetivo="'.$objetivo.'", estatus=1
                                  WHERE id_formato='.$id_formato;
    }else{
      $qry = 'INSERT INTO accionesgrupales (formato,codigo,revision,
                                    fechaExpedicion,id_tutor,id_carrera,
                                    id_grupo,cuatrimestre,horasParcial,
                                    horasSemana,objetivo,director) VALUES
                                   ("'.$formato.'","'.$codigo.'",'.$revision.',
                                    "'.$fechaExpedicion.'",'.$id_tutor.','.$id_carrera.',
                                    '.$id_grupo.',"'.$cuatrimestre.'",'.$horasParcial.',
                                    '.$horasSemana.',"'.$objetivo.'","'.$director.'")';
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      if($id_formato>0){
        //hacer algo si es que se estan editando, puede ser que se agreguen mas, quede igual o se eliminen algunas
      }else{
        $last_id = $con->insert_id;
        foreach ($actividades as $key => $value) {
          $qry = 'INSERT INTO tablaaccionesgrupales
          (id_formato,fechaHoras,actividad,resultados,recursosMateriales,recursosDidacticos)  VALUES
          ('.$last_id.',"'.$value->fechaHoras.'","'.$value->actividad.'","'.$value->resultados.'","'.$value->recursosMateriales.'","'.$value->recursosDidacticos.'")';
          $qry_res = mysqli_query($con,$qry);
          if($qry_res){
            $arr = array('success' => true, 'error' => false);
          }else{
            $arr = array('success' => false,'error' => true);
          }
        }
      }
      // print_r(intval($last_id));
    }
    else{
      $arr = array('success' => false,'error' => true);
    }
    print_r(json_encode($arr));
  };
  function getActividades(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->patg->id_formato);
    $qry = mysqli_query ($con,'SELECT * from tablaaccionesgrupales WHERE id_formato='.$id_formato);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "fechaHoras" => $rows['fechaHoras'],
            "actividad" => $rows['actividad'],
            "resultados" => $rows['resultados'],
            "recursosMateriales" => $rows['recursosMateriales'],
            "recursosDidacticos" => $rows['recursosDidacticos']
        );
    }
    print_r(json_encode($array));
  };
  /**
   	 * Block comment
   	 *
   	 * @param type
   	 * @return void
  ██████   ██████  ██    ██ ██
  ██   ██ ██    ██ ██    ██ ██
  ██████  ██    ██ ██    ██ ██
  ██      ██    ██  ██  ██  ██
  ██       ██████    ████   ██
  */
  function getPOVI(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->usuario->id_usuario);
    $qry = mysqli_query ($con,'SELECT * from programavisitas WHERE id_tutor='.$id_usuario);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "revision" => intval($rows['revision']),
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "numAlumnos" => intval($rows['numAlumnos']),
            "estatus" => intval($rows['estatus']),
            "comentarios" => $rows['comentarios']
        );
    }
    if($array)
    print_r(json_encode($array[0]));
  };
  function setPOVI(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $formato = $data->formato->formato;
    $codigo = $data->formato->codigo;
    $revision = intval($data->formato->revision);
    $fechaExpedicion = $data->formato->fechaExpedicion;
    $id_tutor = intval($data->formato->tutor->id_usuario);
    $id_carrera = intval($data->formato->tutor->carrera->id_carrera);
    $id_grupo = intval($data->formato->tutor->grupo->id_grupo);
    $numAlumnos = intval($data->formato->numAlumnos);
    $director = $data->formato->director;
    $visitas = $data->formato->visitas;
    if($id_formato>0){
      $qry = 'UPDATE programavisitas SET id_tutor='.$id_tutor.',id_carrera='.$id_carrera.',id_grupo='.$id_grupo.',
                                  numAlumnos='.$numAlumnos.', estatus=1
                                  WHERE id_formato='.$id_formato;
    }else{
      $qry = 'INSERT INTO programavisitas (formato,codigo,revision,
                                    fechaExpedicion,id_tutor,id_carrera,
                                    id_grupo,numAlumnos,director) VALUES
                                   ("'.$formato.'","'.$codigo.'",'.$revision.',
                                    "'.$fechaExpedicion.'",'.$id_tutor.','.$id_carrera.',
                                    '.$id_grupo.','.$numAlumnos.',"'.$director.'")';
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      if($id_formato>0){
        //hacer algo si es que se estan editando, puede ser que se agreguen mas, quede igual o se eliminen algunas
      }else{
        $last_id = $con->insert_id;
        foreach ($visitas as $key => $value) {
          $qry = 'INSERT INTO tablaprogramavisitas
          (id_formato,numero,fecha,hora,empresa,objetivo,asignatura,responsable)  VALUES
          ('.$last_id.','.$value->numero.',"'.$value->fecha.'","'.$value->hora.'",
            "'.$value->empresa.'","'.$value->objetivo.'","'.$value->asignatura.'",
            "'.$value->responsable.'")';
          $qry_res = mysqli_query($con,$qry);
          if($qry_res){
            $arr = array('success' => true, 'error' => false);
          }else{
            $arr = array('success' => false,'error' => true);
          }
        }
      }
    }
    else{
      $arr = array('success' => false,'error' => true);
    }
    print_r(json_encode($arr));
  };
  function getVisitas(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->povi->id_formato);
    $qry = mysqli_query ($con,'SELECT * from tablaprogramavisitas WHERE id_formato='.$id_formato);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "numero" => intval($rows['numero']),
            "fecha" => $rows['fecha'],
            "hora" => $rows['hora'],
            "empresa" => $rows['empresa'],
            "objetivo" => $rows['objetivo'],
            "asignatura" => $rows['asignatura'],
            "responsable" => $rows['responsable']
        );
    }
    print_r(json_encode($array));
  };
  /**
   	 * Block comment
   	 *
   	 * @param type
   	 * @return void
  ██ ██████   █████  ████████
  ██ ██   ██ ██   ██    ██
  ██ ██   ██ ███████    ██
  ██ ██   ██ ██   ██    ██
  ██ ██████  ██   ██    ██
  */
  function getIDAT(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->usuario->id_usuario);
    $qry = mysqli_query ($con,'SELECT * from autoevaluaciones WHERE id_tutor='.$id_usuario);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "revision" => intval($rows['revision']),
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "estatus" => intval($rows['estatus']),
            "comentarios" => $rows['comentarios']
        );
    }
    if($array)
    print_r(json_encode($array[0]));
  };
  function setIDAT(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $formato = $data->formato->formato;
    $codigo = $data->formato->codigo;
    $revision = intval($data->formato->revision);
    $fechaExpedicion = $data->formato->fechaExpedicion;
    $id_tutor = intval($data->formato->tutor->id_usuario);
    $id_carrera = intval($data->formato->tutor->carrera->id_carrera);
    $id_grupo = intval($data->formato->tutor->grupo->id_grupo);
    $director = $data->formato->director;
    $preguntas = $data->formato->preguntas;
    if($id_formato>0){
      $qry = 'UPDATE autoevaluaciones SET id_tutor='.$id_tutor.',estatus=1
                                  WHERE id_formato='.$id_formato;
    }else{
      $qry = 'INSERT INTO autoevaluaciones (formato,codigo,revision,
                                    fechaExpedicion,id_tutor,id_carrera,
                                    id_grupo,director) VALUES
                                   ("'.$formato.'","'.$codigo.'",'.$revision.',
                                    "'.$fechaExpedicion.'",'.$id_tutor.','.$id_carrera.',
                                    '.$id_grupo.',"'.$director.'")';
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      if($id_formato>0){
        //hacer algo si es que se estan editando, puede ser que se agreguen mas, quede igual o se eliminen algunas
      }else{
        $last_id = $con->insert_id;
        $numero=1;
        foreach ($preguntas as $key => $value) {
          if($value->comentarios){
            $qry = 'INSERT INTO preguntasautoevaluacion (id_formato,numero,respuesta,comentarios)  VALUES ('.$last_id.','.$numero.',"'.$value->respuesta.'","'.$value->comentarios.'")';
          }else{
            $qry = 'INSERT INTO preguntasautoevaluacion (id_formato,numero,respuesta,comentarios)  VALUES ('.$last_id.','.$numero.',"'.$value->respuesta.'","")';
          }
          $qry_res = mysqli_query($con,$qry);
          if($qry_res){
            $arr = array('success' => true, 'error' => false);
            $numero++;
          }else{
            $arr = array('success' => false,'error->preguntas' => true);
          }
        }
      }
    }
    else{
      $arr = array('success' => false,'error->formato' => true);
    }
    print_r(json_encode($arr));
  };
  function getPreguntas(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->idat->id_formato);
    $qry = mysqli_query ($con,'SELECT * from preguntasautoevaluacion WHERE id_formato='.$id_formato);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "numero" => intval($rows['numero']),
            "respuesta" => $rows['respuesta'],
            "comentarios" => $rows['comentarios']
        );
    }
    print_r(json_encode($array));
  };
?>
