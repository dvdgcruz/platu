<?php
  include("conexion.php");
  header("Content-Type: text/html;charset=utf-8");

  switch ($_REQUEST['action']){
    case 'getDocentes':
      getDocentes();
      break;
    case 'setDesignacion':
      setDesignacion();
      break;
    case 'getDesignaciones':
      getDesignaciones();
      break;
    case 'getPlanes':
      getPlanes();
      break;
    case 'getProgramas':
      getProgramas();
      break;
    case 'getEvaluaciones':
      getEvaluaciones();
      break;
  }

  function getDocentes(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = $data->usuario->id_usuario;
    $qry = mysqli_query ($con,'SELECT * FROM `usuarios` join usuarios_carreras ON usuarios.id_usuario=usuarios_carreras.id_usuario
      AND usuarios.id_usuario!=5 AND (usuarios.tipo=5 OR usuarios.tipo=4) AND usuarios_carreras.id_carrera IN
      (SELECT id_carrera FROM usuarios_carreras where id_usuario='.$id_usuario.') GROUP BY usuarios.id_usuario');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_usuario" => intval($rows['id_usuario']),
            "correo" => $rows['correo'],
            "telefono" => $rows['telefono'],
            "nss" => $rows['nss'],
            "tipo" => intval($rows['tipo']),
            "matricula" => $rows['matricula'],
            "nombreCompleto" => $rows['nombreCompleto'],
            "tiempoCompleto" => intval($rows['tiempoCompleto'])
        );
    }
    print_r(json_encode($array));
  };
  function setDesignacion(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->datos->usuario->id_usuario);
    $id_formato = intval($data->datos->id_formato);
    $revision = intval($data->datos->revision);
    $formato = $data->datos->formato;
    $codigo = $data->datos->codigo;
    $fechaExpedicion = $data->datos->fechaExpedicion;
    $id_tutor = intval($data->datos->tutor->id_usuario);
    $id_carrera = intval($data->datos->carrera->id_carrera);
    $id_grupo = intval($data->datos->grupo->id_grupo);
    $periodo = $data->datos->periodo;

    if($id_formato>0){
      $qry = 'UPDATE designaciones SET id_tutor='.$id_tutor.',id_carrera='.$id_carrera.',
                                  id_grupo='.$id_grupo.',periodo="'.$periodo.'",id_usuario='.$id_usuario.'
                                  WHERE id_formato='.$id_formato;
    }else{
      $qry = 'INSERT INTO designaciones (estatus,formato,codigo,revision,
                                      fechaExpedicion,id_tutor,id_carrera,
                                      id_grupo,periodo,id_usuario) VALUES
                                     (2,"'.$formato.'","'.$codigo.'",'.$revision.',
                                      "'.$fechaExpedicion.'",'.$id_tutor.','.$id_carrera.',
                                      '.$id_grupo.',"'.$periodo.'",'.$id_usuario.')';
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      if($id_formato==0){
        $qry = 'UPDATE usuarios SET tipo=4 WHERE id_usuario='.$id_tutor.'';
        $qry_res = mysqli_query($con,$qry);
        if($qry_res){
          $qry = 'UPDATE grupos SET id_tutor='.$id_tutor.' WHERE id_grupo='.$id_grupo.'';
          $qry_res = mysqli_query($con,$qry);
          if($qry_res){
            $arr = array('success' => true, 'error' => false);
          }else{
            $arr = array('success' => false,'error' => true);
          }
        }else{
          $arr = array('success' => false,'error' => true);
        }
      }
    }else{
      $arr = array('success' => false,'error' => true);
    }
    print_r(json_encode($arr));
  };
  function getDesignaciones(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->id_usuario);
    $qry = mysqli_query ($con,'SELECT * from designaciones WHERE activo=1 AND id_usuario='.$id_usuario);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "revision" => intval($rows['revision']),
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "periodo" => $rows['periodo'],
            "estatus" => intval($rows['estatus'])
        );
    }
    print_r(json_encode($array));
  };
  function getPlanes(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->id_usuario);
    $qry = mysqli_query ($con,'SELECT * FROM accionesgrupales join usuarios ON accionesgrupales.id_tutor=usuarios.id_usuario join usuarios_carreras ON usuarios.id_usuario=usuarios_carreras.id_usuario
      AND usuarios.id_usuario!=5 AND (usuarios.tipo=5 OR usuarios.tipo=4) AND usuarios_carreras.id_carrera IN
      (SELECT id_carrera FROM usuarios_carreras where id_usuario='.$id_usuario.') GROUP BY usuarios.id_usuario');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "estatus" => intval($rows['estatus'])
        );
    }
    print_r(json_encode($array));
  };
  function getProgramas(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->id_usuario);
    $qry = mysqli_query ($con,'SELECT * FROM programavisitas join usuarios ON programavisitas.id_tutor=usuarios.id_usuario join usuarios_carreras ON usuarios.id_usuario=usuarios_carreras.id_usuario
      AND usuarios.id_usuario!=5 AND (usuarios.tipo=5 OR usuarios.tipo=4) AND usuarios_carreras.id_carrera IN
      (SELECT id_carrera FROM usuarios_carreras where id_usuario='.$id_usuario.') GROUP BY usuarios.id_usuario');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "estatus" => intval($rows['estatus'])
        );
    }
    print_r(json_encode($array));
  };
  function getEvaluaciones(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->id_usuario);
    $qry = mysqli_query ($con,'SELECT * FROM autoevaluaciones join usuarios ON autoevaluaciones.id_tutor=usuarios.id_usuario join usuarios_carreras ON usuarios.id_usuario=usuarios_carreras.id_usuario
      AND usuarios.id_usuario!=5 AND (usuarios.tipo=5 OR usuarios.tipo=4) AND usuarios_carreras.id_carrera IN
      (SELECT id_carrera FROM usuarios_carreras where id_usuario='.$id_usuario.') GROUP BY usuarios.id_usuario');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "estatus" => intval($rows['estatus'])
        );
    }
    print_r(json_encode($array));
  };
?>
