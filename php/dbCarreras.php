<?php
  include("conexion.php");
  header("Content-Type: text/html;charset=utf-8");
  switch ($_REQUEST['action']){
    case 'getCarreras':
      getCarreras();
      break;
    case 'getCarrerasPorUsuario':
      getCarrerasPorUsuario();
      break;
    case 'setCarrera':
      setCarrera();
      break;
    case 'dropCarrera':
      dropCarrera();
      break;
  }
  function getCarrerasPorUsuario(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->usuario->id_usuario);
    $qry = mysqli_query ($con,'SELECT carreras.id_carrera,carreras.carrera FROM carreras
      JOIN usuarios_carreras ON carreras.id_carrera=usuarios_carreras.id_carrera
      and usuarios_carreras.id_usuario='.$id_usuario);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
          "id_carrera" => intval($rows['id_carrera']),
          "carrera" => $rows['carrera'],
        );
    }
    print_r(json_encode($array));
  };
  function getCarreras(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from carreras');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
          "id_carrera" => intval($rows['id_carrera']),
          "carrera" => $rows['carrera'],
        );
    }
    print_r(json_encode($array));
  };
  function setCarrera(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_carrera = intval($data->carrera->id_carrera);
    $carrera = $data->carrera->carrera;
    if($id_carrera>0){
      $qry = 'UPDATE carreras SET carrera="'.$carrera.'" WHERE id_carrera='.$id_carrera;
    }else{
      $qry = 'INSERT INTO carreras (carrera) VALUES ("'.$carrera.'")';
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      $last_id = $con->insert_id;
      $arr = array('success' => $last_id, 'error' => false);
    }else{
      $arr = array('success' => false,'error' => true);
    }
    print_r(json_encode($arr));
  };
  function dropCarrera(){
      global $con;
      $data = json_decode(file_get_contents("php://input"));
      $idU = $data->id_carrera;
      $qry_res = mysqli_query($con,'DELETE from carreras WHERE id_carrera='.$idU);
      if($qry_res){
        $arr = array('success' =>true, 'error' => false);
      }else{
        $arr = array('success' =>false,'error' => true);
      }
      print_r(json_encode($arr));
  };
?>
