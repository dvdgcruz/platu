<?php
  include("conexion.php");
  header("Content-Type: text/html;charset=utf-8");

  switch ($_REQUEST['action']){
    case 'getAllDocentes':
      getAllDocentes();
      break;
    case 'getAllDesignaciones':
      getAllDesignaciones();
      break;
    case 'getAllPlanes':
      getAllPlanes();
      break;
    case 'getAllProgramas':
      getAllProgramas();
      break;
    case 'getAllEvaluaciones':
      getAllEvaluaciones();
      break;
    case 'aceptarDesignacion':
      aceptarDesignacion();
      break;
    case 'rechazarDesignacion':
      rechazarDesignacion();
      break;
    case 'aceptarAccionGrupal':
      aceptarAccionGrupal();
      break;
    case 'rechazarAccionGrupal':
      rechazarAccionGrupal();
      break;
    case 'aceptarProgramaVisitas':
      aceptarProgramaVisitas();
      break;
    case 'rechazarProgramaVisitas':
      rechazarProgramaVisitas();
      break;
    case 'aceptarAutoEvaluacion':
      aceptarAutoEvaluacion();
      break;
    case 'rechazarAutoEvaluacion':
      rechazarAutoEvaluacion();
      break;
  }
  function getAllDocentes(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from usuarios WHERE activo=1 AND tipo>2 ');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_usuario" => intval($rows['id_usuario']),
            "correo" => $rows['correo'],
            "telefono" => $rows['telefono'],
            "tipo" => intval($rows['tipo']),
            "matricula" => $rows['matricula'],
            "nombreCompleto" => $rows['nombreCompleto'],
            "tiempoCompleto" => intval($rows['tiempoCompleto']),
        );
    }
    print_r(json_encode($array));
  };
  function getAllDesignaciones(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from designaciones');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "revision" => intval($rows['revision']),
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "periodo" => $rows['periodo'],
            "estatus" => intval($rows['estatus'])
        );
    }
    print_r(json_encode($array));
  };
  function getAllPlanes(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from accionesgrupales');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "revision" => intval($rows['revision']),
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "estatus" => intval($rows['estatus'])
        );
    }
    print_r(json_encode($array));
  };
  function getAllProgramas(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from programavisitas');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "revision" => intval($rows['revision']),
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "estatus" => intval($rows['estatus'])
        );
    }
    print_r(json_encode($array));
  };
  function getAllEvaluaciones(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from autoevaluaciones');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_formato" => intval($rows['id_formato']),
            "formato" => $rows['formato'],
            "codigo" => $rows['codigo'],
            "revision" => intval($rows['revision']),
            "fechaExpedicion" => $rows['fechaExpedicion'],
            "id_tutor" => intval($rows['id_tutor']),
            "id_carrera" => intval($rows['id_carrera']),
            "id_grupo" => intval($rows['id_grupo']),
            "estatus" => intval($rows['estatus'])
        );
    }
    print_r(json_encode($array));
  };
  function aceptarDesignacion(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->id_formato);
    $qry_res = mysqli_query($con,'UPDATE designaciones SET estatus=3 WHERE id_formato='.$id_formato.'');
    if($qry_res){$arr = array('success' => true, 'error' => false);}
    else{$arr = array('success' => false, 'error' => true);}
    print_r(json_encode($arr));
  };
  function rechazarDesignacion(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->id_formato);
    $qry_res = mysqli_query($con,'UPDATE designaciones SET estatus=2 WHERE id_formato='.$id_formato.'');
    if($qry_res){$arr = array('success' => true, 'error' => false);}
    else{$arr = array('success' => false, 'error' => true);}
    print_r(json_encode($arr));
  };
  function aceptarAccionGrupal(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->id_formato);
    $qry_res = mysqli_query($con,'UPDATE accionesgrupales SET estatus=estatus+1 WHERE id_formato='.$id_formato.'');
    if($qry_res){$arr = array('success' => true, 'error' => false);}
    else{$arr = array('success' => false,'error' => true);}
    print_r(json_encode($arr));
  };
  function rechazarAccionGrupal(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $comentarios = $data->formato->comentarios;
    $qry_res = mysqli_query($con,'UPDATE accionesgrupales SET estatus=-1,comentarios="'.$comentarios.'" WHERE id_formato='.$id_formato.'');
    if($qry_res){$arr = array('success' => true, 'error' => false);}
    else{$arr = array('success' => false,'error' => true);}
    print_r(json_encode($arr));
  };
  function aceptarProgramaVisitas(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->id_formato);
    $qry_res = mysqli_query($con,'UPDATE programavisitas SET estatus=estatus+1 WHERE id_formato='.$id_formato.'');
    if($qry_res){$arr = array('success' => true, 'error' => false);}
    else{$arr = array('success' => false,'error' => true);}
    print_r(json_encode($arr));
  };
  function rechazarProgramaVisitas(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $comentarios = $data->formato->comentarios;
    $qry_res = mysqli_query($con,'UPDATE programavisitas SET estatus=-1,comentarios="'.$comentarios.'" WHERE id_formato='.$id_formato.'');
    if($qry_res){$arr = array('success' => true, 'error' => false);}
    else{$arr = array('success' => false,'error' => true);}
    print_r(json_encode($arr));
  };
  function aceptarAutoEvaluacion(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->id_formato);
    $qry_res = mysqli_query($con,'UPDATE autoevaluaciones SET estatus=estatus+1 WHERE id_formato='.$id_formato.'');
    if($qry_res){$arr = array('success' => true, 'error' => false);}
    else{$arr = array('success' => false,'error' => true);}
    print_r(json_encode($arr));
  };
  function rechazarAutoEvaluacion(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_formato = intval($data->formato->id_formato);
    $comentarios = $data->formato->comentarios;
    $qry_res = mysqli_query($con,'UPDATE autoevaluaciones SET estatus=-1,comentarios="'.$comentarios.'" WHERE id_formato='.$id_formato.'');
    if($qry_res){$arr = array('success' => true, 'error' => false);}
    else{$arr = array('success' => false,'error' => true);}
    print_r(json_encode($arr));
  };
?>
