<?php
  include("conexion.php");
  header("Content-Type: text/html;charset=utf-8");
  switch ($_REQUEST['action']){
    case 'getGrupos':
      getGrupos();
      break;
    case 'getGrupoTutorado':
      getGrupoTutorado();
      break;
    case 'setGrupo':
      setGrupo();
      break;
    case 'dropGrupo':
      dropGrupo();
      break;
  }
  function getGrupoTutorado(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->usuario->id_usuario);
    $qry = mysqli_query ($con,'SELECT * from grupos join carreras on carreras.id_carrera=grupos.id_carrera and grupos.id_tutor='.$id_usuario);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[0] = array(
          "id_grupo" => intval($rows['id_grupo']),
          "grupo" => $rows['grupo'],

          "id_area" => intval($rows['id_area']),
          "id_tutor" => intval($rows['id_tutor'])
        );
        $array[1] = array(
          "id_carrera" => intval($rows['id_carrera']),
          "carrera" =>$rows['carrera'],
        );
    }
    print_r(json_encode($array));
  };
  function getGrupos(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from grupos');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
          "id_grupo" => intval($rows['id_grupo']),
          "grupo" => $rows['grupo'],
          "id_carrera" => intval($rows['id_carrera']),
          "id_area" => intval($rows['id_area']),
          "id_tutor" => intval($rows['id_tutor'])
        );
    }
    print_r(json_encode($array));
  };
  function getGruposPorCarrera(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id = $data->usuario->carrera;
    $qry = mysqli_query ($con,'SELECT * from grupos WHERE id_carrera='.$id);
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
          "id_grupo" => $rows['id_grupo'],
          "grupo" => $rows['grupo'],
          "id_carrera" => $rows['id_carrera'],
        );
    }
    print_r(json_encode($array));
  };
  function setGrupo(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_grupo = intval($data->grupo->id_grupo);
    $grupo = $data->grupo->grupo;
    $id_carrera = intval($data->grupo->carrera->id_carrera);
    $id_area = intval($data->grupo->area->id_area);
    if($id_grupo>0){
      $qry = 'UPDATE grupos SET grupo="'.$grupo.'",id_carrera='.$id_carrera.',id_area='.$id_area.' WHERE id_grupo='.$id_grupo;
    }else{
      $qry = 'INSERT INTO grupos (grupo,id_carrera,id_area) VALUES ("'.$grupo.'",'.$id_carrera.','.$id_area.')';
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      $last_id = $con->insert_id;
      $arr = array('success' => $last_id, 'error' => false);
    }else{
      $arr = array('success' => false,'error' => true);
    }
    print_r(json_encode($arr));
  };
  function dropGrupo(){
      global $con;
      $data = json_decode(file_get_contents("php://input"));
      $idU = $data->id_grupo;
      $qry_res = mysqli_query($con,'DELETE from grupos WHERE id_grupo='.$idU);
      if($qry_res){
        $arr = array('success' => true, 'error' => false);
      }else{
        $arr = array('success' => false,'error' => true);
      }
      print_r(json_encode($arr));
  };
?>
