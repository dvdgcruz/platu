<?php
  include("conexion.php");
  header("Content-Type: text/html;charset=utf-8");

  switch ($_REQUEST['action']){
    case 'validaLogin':
      validaLogin();
      break;
    case 'getUsuarios':
      getUsuarios();
      break;
    case 'getAlumnos':
      getAlumnos();
      break;
    case 'setUsuario':
      setUsuario();
      break;
    case 'setAlumno':
      setAlumno();
      break;
    case 'dropUsuario':
      dropUsuario();
      break;
  }
  function validaLogin(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $matricula = $data->usuario->matricula;
    $password = $data->usuario->password;
    $qry = mysqli_query ($con,"SELECT * from usuarios where matricula='".$matricula."' and password='".$password."' AND (tipo>=1 AND tipo<=4)");
    $array = [];
    while($rows = mysqli_fetch_array($qry)){
      $array = [
        "id_usuario" => intval($rows['id_usuario']),
        "matricula" => $rows['matricula'],
        "nombre" => $rows['nombre'],
        "tipo" => intval($rows['tipo']),
        "nombreCompleto" => $rows['nombreCompleto'],
      ];
    }
    print_r(json_encode($array));
  };
  function getAlumnos(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from usuarios JOIN usuarios_grupos ON usuarios.activo=1 AND usuarios.tipo=6 GROUP by usuarios.id_usuario');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_usuario" => intval($rows['id_usuario']),
            "password" => $rows['password'],
            "nombre" => $rows['nombre'],
            "appat" => $rows['appat'],
            "apmat" => $rows['apmat'],
            "curp" => $rows['curp'],
            "matricula" => $rows['matricula'],
            "nombreCompleto" => $rows['nombreCompleto'],
            "carrera" => intval($rows['id_carrera']),
            "grupo" => intval($rows['id_grupo'])
        );
    }
    print_r(json_encode($array));
  };
  function getUsuarios(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $qry = mysqli_query ($con,'SELECT * from usuarios WHERE activo=1 AND tipo>1 and tipo<6');
    $array=array();
    while($rows = mysqli_fetch_array($qry)){
        $array[] = array(
            "id_usuario" => intval($rows['id_usuario']),
            "password" => $rows['password'],
            "activo" => intval($rows['activo']),
            "nombre" => $rows['nombre'],
            "appat" => $rows['appat'],
            "apmat" => $rows['apmat'],
            "curp" => $rows['curp'],
            "correo" => $rows['correo'],
            "telefono" => $rows['telefono'],
            "nss" => $rows['nss'],
            "tipo" => $rows['tipo'],
            "matricula" => $rows['matricula'],
            "fechaCreacion" => $rows['fechaCreacion'],
            "nombreCompleto" => $rows['nombreCompleto'],
            "tiempoCompleto" => intval($rows['tiempoCompleto']),
            "abreviatura" => $rows['abreviatura']
        );
    }
    print_r(json_encode($array));
  };
  function setUsuario(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->usuario->id_usuario);
    $password = $data->usuario->password;
    $nombre = $data->usuario->nombre;
    $appat = $data->usuario->appat;
    $apmat = $data->usuario->apmat;
    $curp = $data->usuario->curp;
    $correo = $data->usuario->correo;
    $telefono = $data->usuario->telefono;
    $nss = $data->usuario->nss;
    $tipo = intval($data->usuario->tipo);
    $matricula = $data->usuario->matricula;
    $fechaCreacion = $data->usuario->fechaCreacion;
    $nombreCompleto = $data->usuario->nombreCompleto;
    $tiempoCompleto = intval($data->usuario->tiempoCompleto);
    $abreviatura = $data->usuario->abreviatura;
    $carreras = $data->usuario->carreras;

    if($id_usuario>0){
      $qry = 'UPDATE usuarios SET password="'.$password.'",tipo='.$tipo.',nombreCompleto="'.$nombreCompleto.'",
                                  nombre="'.$nombre.'",appat="'.$appat.'",apmat="'.$apmat.'",abreviatura="'.$abreviatura.'",
                                  telefono="'.$telefono.'",correo="'.$correo.'",tiempoCompleto='.$tiempoCompleto.',
                                  matricula="'.$matricula.'",curp="'.$curp.'",nss="'.$nss.'"
                                  WHERE id_usuario='.$id_usuario;
      $ban = false;
    }else{
      $qry = 'INSERT INTO usuarios (password,nombre,fechaCreacion,
                                    appat,apmat,curp,nombreCompleto,
                                    abreviatura,tiempoCompleto,
                                    correo,telefono,nss,tipo,matricula) VALUES
                                   ("'.$password.'","'.$nombre.'","'.$fechaCreacion.'",
                                    "'.$appat.'","'.$apmat.'","'.$curp.'","'.$nombreCompleto.'",
                                    "'.$abreviatura.'",'.$tiempoCompleto.',
                                    "'.$correo.'","'.$telefono.'","'.$nss.'",'.$tipo.',"'.$matricula.'")';
      $ban = true;
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      if($ban){
        $id_usuario = $con->insert_id;
      }
      foreach ($carreras as $key => $value) {
        $qry = 'INSERT INTO usuarios_carreras (id_usuario,id_carrera) VALUES ('.$id_usuario.','.$value->id_carrera.')';
        $qry_res = mysqli_query($con,$qry);
          if($qry_res){
            $arr = array('success' => true, 'error' => false);
          }else{
            $arr = array('success' => false,'error' => true);
          }
      }
      $arr = array('success' => true, 'error' => false);
    }
    else{
      $arr = array('success' => false,'error' => true);
    }
    print_r(json_encode($arr));
  };
  function setAlumno(){
    global $con;
    $data = json_decode(file_get_contents("php://input"));
    $id_usuario = intval($data->usuario->id_usuario);
    $password = $data->usuario->password;
    $nombre = $data->usuario->nombre;
    $appat = $data->usuario->appat;
    $apmat = $data->usuario->apmat;
    $curp = $data->usuario->curp;
    $matricula = $data->usuario->matricula;
    $nombreCompleto = $data->usuario->nombreCompleto;
    $tutor = $data->tutor->id_usuario;
    $carrera = $data->tutor->carrera->id_carrera;
    $grupo = $data->tutor->grupo->id_grupo;
    $hoy = date("Y-m-d");

    if($id_usuario>0){
      $qry = 'UPDATE usuarios SET password="'.$password.'",nombreCompleto="'.$nombreCompleto.'",
                                  nombre="'.$nombre.'",appat="'.$appat.'",apmat="'.$apmat.'",
                                  matricula="'.$matricula.'",curp="'.$curp.'"
                                  WHERE id_usuario='.$id_usuario;
    }else{
      $qry = 'INSERT INTO usuarios (password,nombre,fechaCreacion,
                                    appat,apmat,curp,nombreCompleto,
                                    tiempoCompleto,tipo,matricula) VALUES
                                   ("'.$password.'","'.$nombre.'","'.$hoy.'",
                                    "'.$appat.'","'.$apmat.'","'.$curp.'","'.$nombreCompleto.'",
                                    0,6,"'.$matricula.'")';
    }
    $qry_res = mysqli_query($con,$qry);
    if($qry_res){
      $last_id = $con->insert_id;
      $qry = 'INSERT INTO usuarios_grupos (id_grupo,id_usuario,id_carrera,id_tutor) VALUES ('.$grupo.','.$last_id.','.$carrera.','.$tutor.')';
      $qry_res = mysqli_query($con,$qry);
      if($qry_res){
        $arr = array('success' => true, 'error' => false);
      }else{
        $arr = array('success' => false,'error' => true);
      }
    }
    else{
      $arr = array('success' => false,'error' => true);
    }
    print_r(json_encode($arr));
  };
  function dropUsuario(){
      global $con;
      $data = json_decode(file_get_contents("php://input"));
      $idU = $data->id_usuario;
      $qry_res = mysqli_query($con,'UPDATE usuarios SET activo=0 WHERE id_usuario='.$idU);
      if($qry_res){
        $arr = array('success' => true, 'error' => false);
      }
      else{
        $arr = array('success' => false,'error' => true);
      }
      print_r(json_encode($arr));
  };
?>
