app.filter('fecha', [function(value, which) {
  var format = '';
  return function(value, which) {
    switch (which) {
      case 'long':
        format = 'DD [de] MMM [del] YYYY[,] hh:mm a';
        break;
      case 'middle':
        format = 'LL';
        break;
      case 'time':
        format = format = 'hh:mm';
        break;
      default:
        format = 'DD/MM/YYYY';
    }
    return moment(value).locale('es').format(format);
  };
}]);
app.filter('estado', [function() {
  var texto = '';
  return function(value) {
    switch (value) {
      case 1:
        texto = 'Requisitado por Tutor / En Revisión';
        break;
      case 2:
        texto = 'Revisado por Director / En Espera Vo.Bo.';
        break;
      case 3:
        texto = 'Revisado por Coordinación / Aceptado';
        break;
      default:
        texto = 'Modificando';
    }
    return texto;
  };
}]);
app.filter('nombreTipo', [function() {
  var texto = '';
  return function(value) {
    switch (value) {
      case 1:
        texto = 'SuperAdmin';
        break;
      case 2:
        texto = 'Coordinador';
        break;
      case 3:
        texto = 'Director';
        break;
      case 4:
        texto = 'Tutor';
        break;
      case 5:
        texto = 'Docente';
        break;
      case 6:
        texto = 'Alumno';
        break;
      case '1':
        texto = 'SuperAdmin';
        break;
      case '2':
        texto = 'Coordinador';
        break;
      case '3':
        texto = 'Director';
        break;
      case '4':
        texto = 'Tutor';
        break;
      case '5':
        texto = 'Docente';
        break;
      case '6':
        texto = 'Alumno';
        break;
      default:
        texto = 'Usuario';
    }
    return texto;
  };
}]);
app.filter('nombreTutor', [function() {
  return function(value, docentes) {
    var enc = _.find(docentes, {
      id_usuario: value
    });
    if (enc)
      return enc.nombreCompleto;
  };
}]);
app.filter('nombreAlumno', [function() {
  return function(value, alumnos) {
    var enc = _.find(alumnos, {
      id_usuario: value
    });
    if (enc)
      return enc.nombreCompleto;
  };
}]);
app.filter('nombreCarrera', [function() {
  return function(value, carreras) {
    var enc = _.find(carreras,{id_carrera: parseInt(value)});
    if (enc){
      return enc.carrera;
    }
  };
}]);
app.filter('nombreArea', [function() {
  return function(value, areas) {
    var enc = _.find(areas, ['id_area', parseInt(value)]);
    if (enc)
      return enc.area;
  };
}]);
app.filter('nombreGrupo', [function() {
  return function(value, grupos) {
    var enc = _.find(grupos, ['id_grupo', value]);
    if (enc)
      return enc.grupo;
  };
}]);
app.filter('startFromGrid', function() {
  return function(input, start) {
    start = +start;
    return input.slice(start);
  }
});
