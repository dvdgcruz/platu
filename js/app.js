'use strict';
var app = angular.module("Back", [
  'ngRoute',
  'moment-picker',
  'LocalStorageModule',
  'ngWebsocket'
]);
app.factory('auth', function($location) {
  return {
    checkStatus: function() {
      //creamos un array con las rutas que queremos controlar
      var rutasPrivadas = ["/detu", "/usuarios","/grupos","/areas","/alumnos","/formatos","/patg","/","/carreras","/povi","/idat"];
      var rutasSuperUser = ["/usuarios","/carreras","/areas","/grupos"];
      var rutasUsuarios = ["/detu","/patg","/povi","/idat","/alumnos","/formatos"];
      //Si el usuario quiere entrar a alguna de las rutas privadas sin haberse logeado, lo mando al login
      if (this.in_array($location.path(), rutasPrivadas) && typeof(localStorage.usuario) === "undefined") {
        // console.log('localStorage->',typeof(localStorage.usuario));
        $location.path("/login");
      }
      //en el caso de que intente acceder al login y ya haya iniciado sesión lo mandamos a la home
      if ( $location.path()=='/login' && typeof(localStorage.usuario) !== "undefined") {
        $location.path("/");
      }
      if ( $location.path()=='/detu' && typeof(localStorage.usuario) !== "undefined") {
        var usuario = JSON.parse(localStorage.usuario);
        if(usuario.tipo<2 || usuario.tipo>3){$location.path("/");}
      }
      //en el caso de que no sea superuser e intente entrar a una ruta del superuser lo mandamos al dashboard
      if ( this.in_array($location.path(), rutasSuperUser) && typeof(localStorage.usuario) !== "undefined") {
        var usuario = JSON.parse(localStorage.usuario);
        if(usuario.tipo>1){$location.path("/");}
      }
    },
    in_array: function(needle, haystack) {
      var key = '';
      for (key in haystack) {
        if (haystack[key] == needle) {
          return true;
        }
      }
      return false;
    }
  }
});
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'componentes/dashboard.html',
      controller: 'dashboardController'
    })
    .when('/login', {
      templateUrl: 'componentes/usuarios/login.html',
      controller: 'loginController'
    })
    .when('/usuarios', {
      templateUrl: 'componentes/usuarios/usuarios.html',
      controller: 'usuariosController'
    })
    .when('/alumnos', {
      templateUrl: 'componentes/alumnos/alumnos.html',
      controller: 'alumnosController'
    })
    .when('/carreras', {
      templateUrl: 'componentes/carreras/carreras.html',
      controller: 'carrerasController'
    })
    .when('/areas', {
      templateUrl: 'componentes/areas/areas.html',
      controller: 'areasController'
    })
    .when('/grupos', {
      templateUrl: 'componentes/grupos/grupos.html',
      controller: 'gruposController'
    })
    .when('/formatos', {
      templateUrl: 'componentes/formatos/formatos.html',
      controller: 'formatosController'
    })
    .when('/detu', {
      templateUrl: 'componentes/documentos/detu/detu.html',
      controller: 'detuController'
    })
    .when('/patg', {
      templateUrl: 'componentes/documentos/patg/patg.html',
      controller: 'patgController'
    })
    .when('/povi', {
      templateUrl: 'componentes/documentos/povi/povi.html',
      controller: 'poviController'
    })
    .when('/idat', {
      templateUrl: 'componentes/documentos/idat/idat.html',
      controller: 'idatController'
    })
    .when('/rein', {
      templateUrl: 'componentes/documentos/rein/rein.html',
      controller: 'reinController'
    })
    .otherwise({
      redirectTo: '/'
    });
}]);
app.run(['$rootScope','$location','auth', function($rootScope,$location,auth) {
  $rootScope.main = {
    usuario: {}
  };
  $rootScope.guardaUsuario = function(data) {
    localStorage.usuario = JSON.stringify(data);
    $rootScope.main.usuario = data;
    $location.path("/");
  };
  $rootScope.leeUsuario = function() {
    if(localStorage.usuario){
      $rootScope.main.usuario = JSON.parse(localStorage.usuario);
    }
    // console.log('USUARIO ACTUAL->',JSON.parse(localStorage.usuario));
    // $location.path("/");
  };
  $rootScope.leeUsuario();
  $rootScope.quitaUsuario = function() {
    // console.log('removiendo usuario');
    localStorage.removeItem('usuario');
    $rootScope.main.usuario = {};
    $location.path("/");
  };
  $rootScope.$on('$routeChangeStart', function() {
    //llamamos a checkStatus, el cual lo hemos definido en la factoria auth
    //la cuál hemos inyectado en la acción run de la aplicación
    auth.checkStatus();
  });
}]);
