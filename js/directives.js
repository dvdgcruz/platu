app.directive('headerPagina', [function() {
  return {
    restrict: 'E',
    templateUrl: 'componentes/header.html'
  };
}]);
app.directive('encabezadoDirectiva', [function() {
  return {
    scope: {
      datos: '='
    },
    templateUrl: 'componentes/directivas/encabezadoDirectiva.html'
  };
}]);
app.directive('detuDirectiva', [function() {
  return {
    scope: {
      datos: '='
    },
    templateUrl: 'componentes/documentos/detu/detuDirectiva.html'
  };
}]);
app.directive('patgDirectiva', [function() {
  return {
    scope: {
      datos: '='
    },
    templateUrl: 'componentes/documentos/patg/patgDirectiva.html'
  };
}]);
app.directive('actividadDirectiva', [function(){
  return{
    scope: {
      act: '='
    },
    templateUrl: 'componentes/directivas/actividades.html'
  };
}]);
app.directive('poviDirectiva', [function() {
  return {
    scope: {
      datos: '='
    },
    templateUrl: 'componentes/documentos/povi/poviDirectiva.html'
  };
}]);
app.directive('visitaDirectiva', [function(){
  return{
    scope: {
      act: '='
    },
    templateUrl: 'componentes/directivas/visitas.html'
  };
}]);
app.directive('idatDirectiva', [function() {
  return {
    scope: {
      datos: '='
    },
    templateUrl: 'componentes/documentos/idat/idatDirectiva.html'
  };
}]);
app.directive('idat2Directiva', [function() {
  return {
    scope: {
      datos: '='
    },
    templateUrl: 'componentes/documentos/idat/idat2Directiva.html'
  };
}]);
app.directive('reinDirectiva', [function() {
  return {
    scope: {
      datos: '='
    },
    templateUrl: 'componentes/documentos/rein/reinDirectiva.html'
  };
}]);
app.directive('rein2Directiva', [function() {
  return {
    scope: {
      datos: '='
    },
    templateUrl: 'componentes/documentos/rein/rein2Directiva.html'
  };
}]);
