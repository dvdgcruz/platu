-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-04-2019 a las 22:08:30
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tutorias`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accionesgrupales`
--

CREATE TABLE `accionesgrupales` (
  `id_formato` int(11) NOT NULL,
  `formato` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `revision` int(11) NOT NULL,
  `fechaExpedicion` date NOT NULL,
  `id_tutor` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `cuatrimestre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `horasParcial` int(11) NOT NULL,
  `horasSemana` int(11) NOT NULL,
  `objetivo` text COLLATE utf8_unicode_ci NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `director` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentarios` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `accionesgrupales`
--

INSERT INTO `accionesgrupales` (`id_formato`, `formato`, `codigo`, `revision`, `fechaExpedicion`, `id_tutor`, `id_carrera`, `id_grupo`, `cuatrimestre`, `horasParcial`, `horasSemana`, `objetivo`, `estatus`, `director`, `comentarios`) VALUES
(1, 'PLAN DE ACCIÃ“N TUTORIAL GRUPAL', 'FOR-T-10-C', 2, '2019-04-10', 51, 20, 18, 'Segundo', 50, 12, 'Trabajo en equipo', 3, 'Mtro. Alfonso Miguel Escobar', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id_area` int(11) NOT NULL,
  `area` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `id_carrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`id_area`, `area`, `activo`, `id_carrera`) VALUES
(10, 'Sistemas Informaticos', 1, 20),
(11, 'Redes', 0, 22),
(12, 'Mercadotecnia', 1, 26),
(13, 'InnovaciÃ³n', 1, 17),
(14, 'Telecomunicaciones', 1, 20),
(15, 'Sistema de desarrollo multiplataforma', 1, 21),
(16, 'AutomatizaciÃ³n', 1, 23),
(17, 'Paneles Solares', 1, 19),
(18, 'Calentadores Solares', 1, 28);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autoevaluaciones`
--

CREATE TABLE `autoevaluaciones` (
  `id_formato` int(11) NOT NULL,
  `formato` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `revision` int(11) NOT NULL DEFAULT '0',
  `fechaExpedicion` date NOT NULL,
  `codigo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_tutor` int(11) NOT NULL,
  `comentarios` text COLLATE utf8_unicode_ci,
  `director` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_carrera` int(11) DEFAULT NULL,
  `id_grupo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `autoevaluaciones`
--

INSERT INTO `autoevaluaciones` (`id_formato`, `formato`, `estatus`, `revision`, `fechaExpedicion`, `codigo`, `id_tutor`, `comentarios`, `director`, `id_carrera`, `id_grupo`) VALUES
(1, 'INSTRUMENTO DE AUTOEVALUACIÃ“N DIRIGIDO A TUTORES Y TUTORAS', 3, 1, '2019-04-10', 'FOR-T-10-K', 51, NULL, 'Mtro. Alfonso Miguel Escobar', 20, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE `carreras` (
  `id_carrera` int(11) NOT NULL,
  `carrera` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`id_carrera`, `carrera`) VALUES
(17, 'Ing. en Desarrollo de negocios'),
(18, 'Ing. en Procesos alimentarios'),
(19, 'Ing. en EnergÃ­as renovables'),
(20, 'Ing. en TecnologÃ­as de la informaciÃ³n'),
(21, 'T.S.U. en TecnologÃ­as de la informaciÃ³n'),
(22, 'T.S.U. en TecnologÃ­as de la informaciÃ³n y comunicaciÃ³n'),
(23, 'Ing. en MecatrÃ³nica'),
(24, 'Ing. en Agricultura protegida y sustentable'),
(25, 'Lic. en GastronomÃ­a'),
(26, 'T.S.U. en Desarrollo de negocios'),
(27, 'T.S.U. en TecnologÃ­as Biolimentarias'),
(28, 'T.S.U. en EnergÃ­as renovables'),
(29, 'T.S.U. en MecatrÃ³nica'),
(30, 'T.S.U. en Agricultura protegida y sustentable'),
(31, 'T.S.U. en GastronomÃ­a'),
(32, 'T.S.U. en DiseÃ±o textil y moda industrial'),
(33, 'Ing. en DiseÃ±o textil y moda industrial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `designaciones`
--

CREATE TABLE `designaciones` (
  `id_formato` int(11) NOT NULL,
  `formato` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `revision` int(11) NOT NULL,
  `fechaExpedicion` date NOT NULL,
  `id_tutor` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `periodo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `activo` int(11) NOT NULL DEFAULT '1',
  `comentarios` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `designaciones`
--

INSERT INTO `designaciones` (`id_formato`, `formato`, `codigo`, `revision`, `fechaExpedicion`, `id_tutor`, `id_carrera`, `id_grupo`, `periodo`, `id_usuario`, `estatus`, `activo`, `comentarios`) VALUES
(1, 'DesignaciÃ³n de Tutor', 'FOR-T-10-A', 1, '2019-04-06', 51, 20, 18, 'Enero - Abril', 18, 2, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formatos`
--

CREATE TABLE `formatos` (
  `id_formato` int(11) NOT NULL,
  `formato` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `revision` int(11) NOT NULL,
  `fechaExpedicion` date NOT NULL,
  `fechaRevision` date DEFAULT NULL,
  `fechaAceptacion` date DEFAULT NULL,
  `id_tutor` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `comentarios` text COLLATE utf8_unicode_ci,
  `director` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datos` longblob NOT NULL,
  `clave` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `id_alumno` int(11) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `id_grupo` int(11) NOT NULL,
  `grupo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `id_area` int(11) NOT NULL,
  `id_tutor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id_grupo`, `grupo`, `id_carrera`, `id_area`, `id_tutor`) VALUES
(5, 'DE-101', 26, 0, NULL),
(6, 'DE-701', 17, 0, NULL),
(7, 'DE-102', 26, 0, NULL),
(8, 'DE-702', 17, 0, NULL),
(10, 'PA-701', 18, 0, NULL),
(11, 'PA-702', 18, 0, NULL),
(12, 'TB-101', 27, 0, NULL),
(14, 'TB-102', 27, 0, NULL),
(15, 'ER-701', 19, 0, NULL),
(16, 'ER-702', 19, 0, NULL),
(17, 'TI-701', 20, 0, NULL),
(18, 'TI-702', 20, 0, 51),
(19, 'T-501', 22, 0, NULL),
(20, 'T-502', 22, 0, NULL),
(21, 'TI-101', 21, 0, NULL),
(22, 'TI-102', 21, 0, NULL),
(23, 'ME-701', 23, 0, NULL),
(24, 'ME-702', 23, 0, NULL),
(25, 'AG-701', 24, 0, NULL),
(26, 'AG-702', 24, 0, NULL),
(27, 'GA-701', 25, 0, NULL),
(28, 'GA-702', 25, 0, NULL),
(30, 'ME-101', 29, 0, NULL),
(31, 'ME-102', 29, 0, NULL),
(32, 'ER-101', 28, 0, NULL),
(33, 'ER-102', 28, 0, NULL),
(34, 'AG-101', 30, 0, NULL),
(35, 'AG-102', 30, 0, NULL),
(36, 'GA-101', 31, 0, NULL),
(37, 'GA-102', 31, 0, NULL),
(38, 'DT-101', 32, 0, NULL),
(39, 'DT-102', 32, 0, NULL),
(40, 'DT-701', 33, 0, NULL),
(41, 'DT-702', 33, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntasautoevaluacion`
--

CREATE TABLE `preguntasautoevaluacion` (
  `id_formato` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `respuesta` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentarios` text COLLATE utf8_unicode_ci,
  `indice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `preguntasautoevaluacion`
--

INSERT INTO `preguntasautoevaluacion` (`id_formato`, `numero`, `respuesta`, `comentarios`, `indice`) VALUES
(1, 1, '[2]', '', 1),
(1, 2, '[3]', '', 2),
(1, 3, '[3]', '', 3),
(1, 4, '[4]', '', 4),
(1, 5, '[2]', '', 5),
(1, 6, '[3]', '', 6),
(1, 7, '[3]', '', 7),
(1, 8, '[2]', '', 8),
(1, 9, '[2]', '', 9),
(1, 10, '[3]', '', 10),
(1, 11, '[3]', '', 11),
(1, 12, '[3]', '', 12),
(1, 13, '[3]', '', 13),
(1, 14, '[2]', '', 14),
(1, 15, '[2]', '', 15),
(1, 16, '[4]', '', 16),
(1, 17, '[1]', '', 17),
(1, 18, '[3]', '', 18),
(1, 19, '[3]', '', 19),
(1, 20, '[4]', '', 20),
(1, 21, '[1]', '', 21),
(1, 22, '[4]', '', 22),
(1, 23, '[3]', '', 23),
(1, 24, '[3]', '', 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntasreporteindividual`
--

CREATE TABLE `preguntasreporteindividual` (
  `indice` int(11) NOT NULL,
  `id_formato` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `respuesta` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `preguntasreporteindividual`
--

INSERT INTO `preguntasreporteindividual` (`indice`, `id_formato`, `numero`, `respuesta`) VALUES
(1, 1, 0, 'qwe'),
(2, 1, 1, 'Si'),
(3, 1, 2, '23'),
(4, 1, 3, 'qwe'),
(5, 1, 4, 'qw'),
(6, 1, 5, 'Si'),
(7, 1, 6, '23'),
(8, 1, 7, 'qw'),
(9, 1, 8, '1'),
(10, 1, 9, 'qwe'),
(11, 1, 10, 'qwe'),
(12, 1, 11, 'qwe'),
(13, 1, 12, 'Si'),
(14, 1, 13, 'qwe'),
(15, 1, 14, 'Si'),
(16, 1, 15, 'qwe'),
(17, 1, 16, 'qwe'),
(18, 1, 17, 'qwe'),
(19, 1, 18, 'qwe'),
(20, 1, 19, 'qwe'),
(21, 1, 20, 'qwe'),
(22, 1, 21, 'qwe'),
(23, 1, 22, 'qwe'),
(24, 1, 23, 'qwe'),
(25, 1, 24, 'qwe'),
(26, 1, 25, 'qwe'),
(27, 1, 26, 'qwe'),
(28, 1, 27, 'qwe'),
(29, 1, 28, 'qwe'),
(30, 1, 29, 'qwe'),
(31, 1, 30, 'qwe'),
(32, 1, 31, 'qwe'),
(33, 1, 32, 'qwe'),
(34, 1, 33, 'qwe'),
(35, 1, 34, 'qwe'),
(36, 1, 35, 'qwe'),
(37, 1, 36, 'qwe'),
(38, 1, 37, 'qwe'),
(39, 1, 38, 'qwe'),
(40, 1, 39, 'qwe'),
(41, 1, 40, 'qwe'),
(42, 1, 41, 'qwe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programavisitas`
--

CREATE TABLE `programavisitas` (
  `id_formato` int(11) NOT NULL,
  `formato` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `revision` int(11) NOT NULL,
  `fechaExpedicion` date NOT NULL,
  `id_tutor` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `numAlumnos` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `director` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentarios` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `programavisitas`
--

INSERT INTO `programavisitas` (`id_formato`, `formato`, `codigo`, `revision`, `fechaExpedicion`, `id_tutor`, `id_carrera`, `id_grupo`, `numAlumnos`, `estatus`, `director`, `comentarios`) VALUES
(1, 'PROGRAMA DE VISITAS', 'FOR-VE-08-A', 1, '2019-04-10', 51, 20, 18, 24, 3, 'Mtro. Alfonso Miguel Escobar', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportesindividuales`
--

CREATE TABLE `reportesindividuales` (
  `id_formato` int(11) NOT NULL,
  `formato` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `revision` int(11) NOT NULL,
  `fechaExpedicion` date NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `comentarios` text COLLATE utf8_unicode_ci,
  `fechaRevision` date DEFAULT NULL,
  `id_tutor` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  `director` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `reportesindividuales`
--

INSERT INTO `reportesindividuales` (`id_formato`, `formato`, `codigo`, `revision`, `fechaExpedicion`, `estatus`, `comentarios`, `fechaRevision`, `id_tutor`, `id_carrera`, `id_grupo`, `id_alumno`, `director`) VALUES
(1, 'INSTRUMENTO DE AUTOEVALUACIÃ“N DIRIGIDO A TUTORES Y TUTORAS', 'FOR-T-10-K', 1, '2019-04-07', 1, NULL, NULL, 51, 20, 18, 73, 'Mtro. Alfonso Miguel Escobar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tablaaccionesgrupales`
--

CREATE TABLE `tablaaccionesgrupales` (
  `id_formato` int(11) NOT NULL,
  `fechaHoras` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `actividad` text COLLATE utf8_unicode_ci NOT NULL,
  `resultados` text COLLATE utf8_unicode_ci NOT NULL,
  `recursosMateriales` text COLLATE utf8_unicode_ci NOT NULL,
  `recursosDidacticos` text COLLATE utf8_unicode_ci NOT NULL,
  `indice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tablaaccionesgrupales`
--

INSERT INTO `tablaaccionesgrupales` (`id_formato`, `fechaHoras`, `actividad`, `resultados`, `recursosMateriales`, `recursosDidacticos`, `indice`) VALUES
(1, '12-08-2019', 'Corrrer', 'Trabajo en equipo', 'Cintas', 'Libros', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tablaprogramavisitas`
--

CREATE TABLE `tablaprogramavisitas` (
  `id_formato` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` text COLLATE utf8_unicode_ci NOT NULL,
  `objetivo` text COLLATE utf8_unicode_ci NOT NULL,
  `asignatura` text COLLATE utf8_unicode_ci NOT NULL,
  `responsable` text COLLATE utf8_unicode_ci NOT NULL,
  `indice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `tablaprogramavisitas`
--

INSERT INTO `tablaprogramavisitas` (`id_formato`, `numero`, `fecha`, `hora`, `empresa`, `objetivo`, `asignatura`, `responsable`, `indice`) VALUES
(1, 1, '2019-04-10', '09:32', 'Bits and Bytes', 'asladjh', 'Desarrolllo', 'Ing. esli', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `password` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `activo` tinyint(4) NOT NULL DEFAULT '1',
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `appat` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `apmat` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` mediumtext COLLATE utf8_unicode_ci,
  `correo` mediumtext COLLATE utf8_unicode_ci,
  `matricula` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `curp` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nss` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombreCompleto` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiempoCompleto` tinyint(4) NOT NULL DEFAULT '1',
  `abreviatura` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `lugarNacimiento` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `municipio` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lugarResidencia` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nivel` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuatrimestre` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vive` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `password`, `tipo`, `fechaCreacion`, `activo`, `nombre`, `appat`, `apmat`, `telefono`, `correo`, `matricula`, `curp`, `nss`, `nombreCompleto`, `tiempoCompleto`, `abreviatura`, `edad`, `lugarNacimiento`, `municipio`, `estado`, `lugarResidencia`, `nivel`, `cuatrimestre`, `vive`) VALUES
(1, 'topos123', 1, '2019-02-03', 1, 'Superadmin', 'a', 'a', NULL, NULL, 'utti123123', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '123', 2, '2019-04-02', 1, 'Gisela', 'Blas', 'PiÃ±Ã³n', '951 211 4091', 'giselabp09@gmail.com', 'UTT0001', 'x', 'x', 'Mtra. Gisela Blas PiÃ±Ã³n', 1, 'Mtra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '123', 3, '2019-04-02', 1, 'Diego', 'Zarate', 'Velasco', '951 124 9007', 'diegozave46@hotmail.com', 'UTT0002', 'x', 'x', 'Mtro. Diego Zarate Velasco', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '123', 3, '2019-04-02', 1, 'Rufino Fidel', 'SÃ¡nchez', 'Maqueo', '951 134 3353', 'rfsmaq54@yahoo.com.mx', 'UTT0003', 'x', 'x', 'Mtro. Rufino Fidel SÃ¡nchez Maqueo', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '123', 3, '2019-04-02', 1, 'Marcos Fredy', 'Morales', 'Celaya', '951 510 3683', 'mfmorcel@gmail.com', 'UTT0004', 'x', 'x', 'Mtro. Marcos Fredy Morales Celaya', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '123', 3, '2019-04-02', 1, 'Alfonso', 'Miguel', 'Escobar', '951 127 9898', 'alfonsoske@gmail.com', 'UTT0005', 'x', 'x', 'Mtro. Alfonso Miguel Escobar', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '123', 3, '2019-04-02', 1, 'David', 'SigÃ¼enza', 'Paz', '222 114 2235', 'chefdavidsiguenza@hotmail.com', 'UTT0006', 'x', 'x', 'Chef. David SigÃ¼enza Paz', 1, 'Chef.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '123', 3, '2019-04-02', 1, 'Alberto', 'Mijangos', 'HernÃ¡ndez', '951 131 88 23', 'mijangos70@hotmail.com', 'UTT0007', 'x', 'x', 'Mtro. Alberto Mijangos HernÃ¡ndez', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '123', 5, '2019-04-02', 1, 'Lizbeth', 'ChacÃ³n', 'x', '951 119 1997', 'liz.chaconmtz@hotmail.com', 'UTT0008', 'x', 'x', 'Mtra. Lizbeth ChacÃ³n x', 1, 'Mtra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '123', 5, '2019-04-02', 1, 'Gladys', 'Pizarro', 'Altamirano', '951 255 2780', 'gla_piz@hotmail.com', 'UTT0009', 'x', 'x', 'Mtra. Gladys Pizarro Altamirano', 1, 'Mtra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '123', 5, '2019-04-02', 1, 'Laura Thaigi', 'Mendoza', 'GÃ³mez', '951 117 9452', 'thaigi17@hotmail.com', 'UTT0010', 'x', 'x', 'Mtra. Laura Thaigi Mendoza GÃ³mez', 1, 'Mtra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '123', 5, '2019-04-02', 1, 'Erika', 'Cruz', 'Estudillo', '951 160 2525', 'akire.ce@hotmail.com', 'UTT0011', 'x', 'x', 'Mtra. Erika Cruz Estudillo', 1, 'Mtra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '123', 5, '2019-04-02', 1, 'NÃ©stor JehovÃ¡', 'Luna', 'Santiago', '951 199 7266', 'lsnestor@hotmail.com', 'UTT0012', 'x', 'x', 'Mtro. NÃ©stor JehovÃ¡ Luna Santiago', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, '123', 5, '2019-04-02', 1, 'Roberto', 'Vicente', 'Yescas', '951 156 5550', 'yescas_vicente@hotmail.com', 'UTT0013', 'x', 'x', 'Mtro. Roberto Vicente Yescas', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, '123', 5, '2019-04-02', 1, 'Gustavo', 'PÃ©rez', 'Barroso', '951 156 4453', 'g_perez_barroso@yahoo.com.mx', 'UTT0014', 'x', 'x', 'Mtro. Gustavo PÃ©rez Barroso', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, '123', 5, '2019-04-02', 1, 'Maribel', 'Torres', 'GÃ³mez', '951 242 7261', 'mtorres_utvco@live.com.mx', 'UTT0015', 'x', 'x', 'Mtra. Maribel Torres GÃ³mez', 1, 'Mtra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, '123', 5, '2019-04-02', 1, 'Itzel', 'Blanco', 'Caballero', '951 228 6568', 'itzelblancoc@hotmail.com', 'UTT0016', 'x', 'x', 'Mtra. Itzel Blanco Caballero', 1, 'Mtra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, '123', 5, '2019-04-02', 1, 'Daniela Patricia', 'Santiago', 'IbÃ¡Ã±ez', '951 214 5535', 'dannypatty03@gmail.com', 'UTT0017', 'x', 'x', 'Dra. Daniela Patricia Santiago IbÃ¡Ã±ez', 1, 'Dra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, '123', 5, '2019-04-02', 1, 'Alejandro', 'Alderete', 'Nava', '951 209 4771', 'alde-nava@hotmail.com', 'UTT0018', 'x', 'x', 'Mtro. Alejandro Alderete Nava', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '123', 5, '2019-04-02', 1, 'RÃ¡ul', 'Acevedo', 'Concha', '951 148 1247', 'raulalejandro21@hotmail.com', 'UTT0019', 'x', 'x', 'Mtro. RÃ¡ul Acevedo Concha', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, '123', 5, '2019-04-02', 1, 'IvÃ¡n Jail', 'SÃ¡mchez', 'LÃ³pez', '951 243 6594', 'jalil_sl@hotmail.com', 'UTT0020', 'x', 'x', 'Mtro. IvÃ¡n Jail SÃ¡mchez LÃ³pez', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, '123', 5, '2019-04-02', 1, 'Fray Guadalupe', 'GarcÃ­a', 'PÃ©rez', '951 203 1262', 'fray_08274@outlook.com', 'UTT0021', 'x', 'x', 'Mtro. Fray Guadalupe GarcÃ­a PÃ©rez', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, '123', 5, '2019-04-02', 1, 'Juan', 'Urbano', 'Ramos', '951 509 8505', 'urbajuan@hotmail.com', 'UTT0022', 'x', 'x', 'Mtro. Juan Urbano Ramos', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, '123', 5, '2019-04-02', 1, 'Luis Miguel', 'Silva', 'PÃ©rez', '951 189 0789', 'miguelsilvaperez21@gmail.com', 'UTT0023', 'x', 'x', 'Mtro. Luis Miguel Silva PÃ©rez', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, '123', 5, '2019-04-02', 1, 'CÃ©sar', 'RamÃ­rez', 'LÃ³pez', '951 168 0595', 'cesar_rl_7@hotmail.com', 'UTT0024', 'x', 'x', 'Mtro. CÃ©sar RamÃ­rez LÃ³pez', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, '123', 5, '2019-04-02', 1, 'Augusto', 'Mosqueda', 'SolÃ­s', '5568019444', 'amsrenovables@gmail.com', 'UTT0025', 'x', 'x', 'Mtro. Augusto Mosqueda SolÃ­s', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, '123', 5, '2019-04-02', 1, 'Carlos', 'FernÃ¡ndez', 'RÃ­os', '951 111 6433', 'cfernandezrios1@gmail.com', 'UTT0026', 'x', 'x', 'Mtro. Carlos FernÃ¡ndez RÃ­os', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, '123', 5, '2019-04-02', 1, 'Javier Daniel', 'RamÃ­rez', 'Amaya', '951 267 2936', 'iqjdra@gmail.com', 'UTT0027', 'x', 'x', 'Mtro. Javier Daniel RamÃ­rez Amaya', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, '123', 5, '2019-04-02', 1, 'Jorge Alejandor', 'Santiago', 'Urbina', '554 043 8639', 'jorsau84@gmail.com', 'UTT0028', 'x', 'x', 'Dr. Jorge Alejandor Santiago Urbina', 1, 'Dr.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, '123', 5, '2019-04-02', 1, 'Salvador', 'Aguilar', 'HernÃ¡ndez', '951 278 09 41', 'salvadoraguilar22@hotmail.es', 'UTT0029', 'x', 'x', 'Ing. Salvador Aguilar HernÃ¡ndez', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, '123', 5, '2019-04-02', 1, 'Karla Jeanett', 'TomÃ¡s', 'SebastiÃ¡n', '951 184 8908', 'kartomseb@hotmail.com', 'UTT0030', 'x', 'x', 'Ing. Karla Jeanett TomÃ¡s SebastiÃ¡n', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, '123', 5, '2019-04-02', 1, 'Antonio', 'Moreno', 'NIÃ±o', '953 114 6419', 'moreno_5696@hotmail.com', 'UTT0031', 'x', 'x', 'M.C. Antonio Moreno NIÃ±o', 1, 'M.C.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, '123', 5, '2019-04-02', 1, 'Marta G.', 'Andrez', 'Morales', '595 109 9555', 'atram107@hotmail.com', 'UTT0032', 'x', 'x', 'Mtra. Marta G. Andrez Morales', 1, 'Mtra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, '123', 5, '2019-04-02', 1, 'Evelio', 'GonzÃ¡lez', 'SÃ¡nchez', '537 206 8167', 'eveliogonsa@yahoo.es', 'UTT0033', 'x', 'x', 'Mtro. Evelio GonzÃ¡lez SÃ¡nchez', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, '123', 5, '2019-04-02', 1, 'Uziel Israel', 'LÃ³pez', 'Garcia', '951 313 95 75', 'loguzziel@hotmail.com', 'UTT0034', 'x', 'x', 'Ing. Uziel Israel LÃ³pez Garcia', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, '123', 5, '2019-04-02', 1, 'Laura', 'HernÃ¡ndez', 'Cruz', '595 108 12 79', 'Laurahdez1@gmail.com', 'UTT0035', 'x', 'x', 'Dra. Laura HernÃ¡ndez Cruz', 1, 'Dra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, '123', 5, '2019-04-02', 1, 'Daniela', 'HernÃ¡ndez', 'Cortez', '951 180 4594', 'ynad2912@outlook.com', 'UTT0036', 'x', 'x', 'Psic. Daniela HernÃ¡ndez Cortez', 1, 'Psic.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, '123', 5, '2019-04-02', 1, 'MoisÃ©s AdriÃ¡n', 'HernÃ¡ndez', 'Luis', '951 104 3505', 'mahl107@hotmail.com', 'UTT0037', 'x', 'x', 'Mtro. MoisÃ©s AdriÃ¡n HernÃ¡ndez Luis', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, '123', 4, '2019-04-02', 1, 'Esli Paola', 'HernÃ¡ndez', 'Blas', '951 148 1088', 'eslypaola21@gmail.com', 'UTT0038', 'x', 'x', 'Ing. Esli Paola HernÃ¡ndez Blas', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, '123', 5, '2019-04-02', 1, 'Gerardo', 'Bautista', 'Morales', '951 118 9875', 'ger_bau24@hotmail.com', 'UTT0039', 'x', 'x', 'Mtro. Gerardo Bautista Morales', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, '123', 5, '2019-04-02', 1, 'Alejandro JesÃºs', 'Morales', 'PÃ©rez', '951 135 6709', 'li.alejandro.mp@gmail.com', 'UTT0040', 'x', 'x', 'Mtro. Alejandro JesÃºs Morales PÃ©rez', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, '123', 5, '2019-04-02', 1, 'JosÃ© Alfredo', 'Vicente', 'Vicente', '951 106 7115', 'Josevicente9101@gmail.com', 'UTT0041', 'x', 'x', 'Ing. JosÃ© Alfredo Vicente Vicente', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, '123', 5, '2019-04-02', 1, 'Aldo Christian', 'AlmarÃ¡z', 'LÃ³pez', '951 117 4670', 'aldo_almaraz_lopez@hotmail.com', 'UTT0042', 'x', 'x', 'Mtro. Aldo Christian AlmarÃ¡z LÃ³pez', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, '123', 5, '2019-04-02', 1, 'Jorge Felipe', 'PÃ©rez', 'Mendoza', '951 209 8553', 'jfelipe.pm@gmail.com', 'UTT0043', 'x', 'x', 'Ing. Jorge Felipe PÃ©rez Mendoza', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, '123', 5, '2019-04-02', 1, 'Gonzalo', 'Berra', 'Ruiz', '951 240 4648', 'berraruizgo@live.com.mx', 'UTT0044', 'x', 'x', 'Ing. Gonzalo Berra Ruiz', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, '123', 5, '2019-04-02', 1, 'Arciris Eleanto', 'Figueroa', 'Sumano', '951 111 0559', 'arcirisfs@gmail.com', 'UTT0045', 'x', 'x', 'Ing. Arciris Eleanto Figueroa Sumano', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, '123', 5, '2019-04-02', 1, 'CÃ©sar Andres', 'Rosales', 'Torres', '55 2909 9417', 'cesarart90@gmail.com', 'UTT0046', 'x', 'x', 'Ing. CÃ©sar Andres Rosales Torres', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, '123', 5, '2019-04-02', 1, 'Alberto', 'Aquino', 'Arango', '951 269 9032', 'arqbetoaa@gmail.com', 'UTT0047', 'x', 'x', 'Mtro. Alberto Aquino Arango', 1, 'Mtro.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, '123', 5, '2019-04-02', 1, 'Marcelino', 'MartÃ­nez', 'AragÃ³n', '951 288 5683', 'mtz.marcelino@gmail.com', 'UTT0048', 'x', 'x', 'Ing. Marcelino MartÃ­nez AragÃ³n', 1, 'Ing.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, '123', 5, '2019-04-02', 1, 'Ana Lorena', 'Figueroa', 'Montelongo', '222 118 6758', 'chef.lorenafigueroam@hotmail.com', 'UTT0049', 'x', 'x', 'M.A. Ana Lorena Figueroa Montelongo', 1, 'M.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, '123', 5, '2019-04-02', 1, 'Victor Hugo', 'CantÃ³n', 'Flores', '951 127 2484', 'hugocanton87@hotmail.com', 'UTT0050', 'x', 'x', 'L.G. Victor Hugo CantÃ³n Flores', 1, 'L.G.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, '123', 5, '2019-04-02', 1, 'Gabriel Stephany', 'RamÃ­rez', 'GarcÃ­a', '951 177 8691', 'ybagarmz_89@hotmail.com', 'UTT0051', 'x', 'x', 'M.C.E. Gabriel Stephany RamÃ­rez GarcÃ­a', 1, 'M.C.E.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, '123', 5, '2019-04-02', 1, 'JosÃ© Antonio', 'JuarÃ©z', 'VÃ¡squez', '951 131 7116', 'basbarummster@gmail.com', 'UTT0052', 'x', 'x', 'L.G. JosÃ© Antonio JuarÃ©z VÃ¡squez', 1, 'L.G.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, '123', 5, '2019-04-02', 1, 'JosÃ© Antonio', 'VillafaÃ±e', 'GonzÃ¡lez', '555 064 7522', 'joseantonio.villafane@gmail.com', 'UTT0053', 'x', 'x', 'L.G. JosÃ© Antonio VillafaÃ±e GonzÃ¡lez', 1, 'L.G.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, '132', 5, '2019-04-02', 1, 'Carolina', 'Fabian', 'Agamas', '951 141 1770', 'chefcarolina.agamas@gmail.com', 'UTT0054', 'x', 'x', 'L.G. Carolina Fabian Agamas', 1, 'L.G.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, '123', 5, '2019-04-02', 1, 'Miguel Ãngel', 'Ãvila', 'Arjona', '951 312 8400', 'mavilaarjona@gmail.com', 'UTT0055', 'x', 'x', 'L.G. Miguel Ãngel Ãvila Arjona', 1, 'L.G.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, '123', 5, '2019-04-02', 1, 'XhonaneÃ© Soledad', 'JimÃ©nez', 'Eugenio', '951-255-4195', 'xhonanee.jmz@gmail.com', 'UTT0056', 'x', 'x', 'L.G. XhonaneÃ© Soledad JimÃ©nez Eugenio', 1, 'L.G.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, '123', 5, '2019-04-02', 1, 'Guadalupe Anai', 'MachaÃ­n', 'Becerra', '9511266746', 'anai.machai.becerra@gmail.com', 'UTT0057', 'x', 'x', 'Mtra. Guadalupe Anai MachaÃ­n Becerra', 1, 'Mtra.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, '123', 5, '2019-04-02', 1, 'Soledad', 'Amalia', 'Cruz', '951-183-7438', 'soledad.amalia@gmail.com', 'UTT0058', 'Velasco', 'x', 'M.A. Soledad Amalia Cruz', 1, 'M.A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, '123', 5, '2019-04-02', 1, 'VerÃ³nica Elizabeth', 'Trujillo', 'MartÃ­nez', '951 164 9210', 'verog9oax@gmail.com', 'UTT0059', 'x', 'x', 'M.C. VerÃ³nica Elizabeth Trujillo MartÃ­nez', 1, 'M.C.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 'qwe', 6, '2019-04-07', 1, 'qwe', 'qwe', 'qwe', 'qwe', NULL, 'QWE', 'QWE', NULL, 'qwe qwe qwe', 0, NULL, 12, 'qwe', 'qwe', 'qwe', 'qwe', 'Lic', 'Mayo - Agosto', ''),
(74, 'asd', 6, '2019-04-07', 1, 'asd', 'asd', 'asd', NULL, NULL, 'ASD', 'ASD', NULL, 'asd asd asd', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_carreras`
--

CREATE TABLE `usuarios_carreras` (
  `id_usuario` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `indice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios_carreras`
--

INSERT INTO `usuarios_carreras` (`id_usuario`, `id_carrera`, `indice`) VALUES
(15, 17, 24),
(15, 26, 25),
(20, 32, 26),
(20, 33, 27),
(20, 32, 28),
(20, 33, 29),
(20, 32, 30),
(20, 33, 31),
(19, 25, 32),
(19, 31, 33),
(18, 20, 34),
(18, 21, 35),
(18, 22, 36),
(18, 23, 37),
(18, 29, 38),
(17, 18, 39),
(17, 24, 40),
(17, 27, 41),
(17, 30, 42),
(16, 19, 43),
(16, 28, 44),
(15, 17, 45),
(15, 26, 46),
(15, 17, 47),
(15, 26, 48),
(21, 17, 49),
(21, 26, 50),
(22, 17, 51),
(22, 26, 52),
(23, 17, 53),
(23, 26, 54),
(24, 17, 55),
(24, 26, 56),
(25, 17, 57),
(25, 26, 58),
(26, 17, 59),
(26, 26, 60),
(27, 17, 61),
(27, 26, 62),
(28, 17, 63),
(28, 26, 64),
(29, 17, 65),
(29, 26, 66),
(30, 17, 67),
(30, 26, 68),
(31, 19, 69),
(31, 28, 70),
(32, 19, 71),
(32, 28, 72),
(33, 19, 73),
(33, 28, 74),
(34, 19, 75),
(34, 28, 76),
(35, 19, 77),
(35, 28, 78),
(36, 19, 79),
(36, 28, 80),
(37, 19, 81),
(37, 28, 82),
(38, 19, 83),
(38, 28, 84),
(39, 18, 85),
(39, 27, 86),
(40, 18, 87),
(40, 27, 88),
(41, 18, 89),
(41, 27, 90),
(42, 18, 91),
(42, 27, 92),
(43, 18, 93),
(43, 27, 94),
(44, 18, 95),
(44, 27, 96),
(45, 24, 97),
(45, 30, 98),
(46, 24, 99),
(46, 30, 100),
(47, 24, 101),
(47, 30, 102),
(48, 24, 103),
(48, 30, 104),
(49, 20, 105),
(49, 21, 106),
(49, 22, 107),
(50, 20, 108),
(50, 21, 109),
(50, 22, 110),
(51, 20, 111),
(51, 21, 112),
(51, 22, 113),
(52, 20, 114),
(52, 21, 115),
(52, 22, 116),
(53, 20, 117),
(53, 21, 118),
(53, 22, 119),
(53, 20, 120),
(53, 21, 121),
(53, 22, 122),
(53, 20, 123),
(53, 21, 124),
(53, 22, 125),
(54, 20, 126),
(54, 21, 127),
(54, 22, 128),
(55, 23, 129),
(55, 29, 130),
(56, 23, 131),
(56, 29, 132),
(57, 23, 133),
(57, 29, 134),
(58, 23, 135),
(58, 29, 136),
(59, 23, 137),
(59, 29, 138),
(61, 23, 139),
(61, 29, 140),
(60, 23, 141),
(60, 29, 142),
(59, 23, 143),
(59, 29, 144),
(59, 23, 145),
(59, 29, 146),
(62, 25, 147),
(62, 31, 148),
(63, 25, 149),
(63, 31, 150),
(64, 25, 151),
(64, 31, 152),
(65, 25, 153),
(65, 31, 154),
(66, 25, 155),
(66, 31, 156),
(67, 25, 157),
(67, 31, 158),
(68, 25, 159),
(68, 31, 160),
(69, 25, 161),
(69, 31, 162),
(70, 25, 163),
(70, 31, 164),
(71, 25, 165),
(71, 31, 166),
(72, 25, 167),
(72, 31, 168);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_grupos`
--

CREATE TABLE `usuarios_grupos` (
  `indice` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `id_tutor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios_grupos`
--

INSERT INTO `usuarios_grupos` (`indice`, `id_grupo`, `id_usuario`, `id_carrera`, `id_tutor`) VALUES
(8, 28, 73, 25, 64),
(9, 28, 74, 25, 64),
(10, 6, 75, 17, 23),
(11, 6, 76, 17, 23),
(12, 18, 73, 20, 51),
(13, 18, 74, 20, 51);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accionesgrupales`
--
ALTER TABLE `accionesgrupales`
  ADD PRIMARY KEY (`id_formato`);

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id_area`);

--
-- Indices de la tabla `autoevaluaciones`
--
ALTER TABLE `autoevaluaciones`
  ADD PRIMARY KEY (`id_formato`);

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`id_carrera`);

--
-- Indices de la tabla `designaciones`
--
ALTER TABLE `designaciones`
  ADD PRIMARY KEY (`id_formato`);

--
-- Indices de la tabla `formatos`
--
ALTER TABLE `formatos`
  ADD PRIMARY KEY (`id_formato`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id_grupo`);

--
-- Indices de la tabla `preguntasautoevaluacion`
--
ALTER TABLE `preguntasautoevaluacion`
  ADD PRIMARY KEY (`indice`);

--
-- Indices de la tabla `preguntasreporteindividual`
--
ALTER TABLE `preguntasreporteindividual`
  ADD PRIMARY KEY (`indice`);

--
-- Indices de la tabla `programavisitas`
--
ALTER TABLE `programavisitas`
  ADD PRIMARY KEY (`id_formato`);

--
-- Indices de la tabla `reportesindividuales`
--
ALTER TABLE `reportesindividuales`
  ADD PRIMARY KEY (`id_formato`);

--
-- Indices de la tabla `tablaaccionesgrupales`
--
ALTER TABLE `tablaaccionesgrupales`
  ADD PRIMARY KEY (`indice`),
  ADD KEY `indice` (`indice`);

--
-- Indices de la tabla `tablaprogramavisitas`
--
ALTER TABLE `tablaprogramavisitas`
  ADD PRIMARY KEY (`indice`),
  ADD KEY `indice` (`indice`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `usuarios_carreras`
--
ALTER TABLE `usuarios_carreras`
  ADD PRIMARY KEY (`indice`),
  ADD KEY `indice` (`indice`);

--
-- Indices de la tabla `usuarios_grupos`
--
ALTER TABLE `usuarios_grupos`
  ADD PRIMARY KEY (`indice`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accionesgrupales`
--
ALTER TABLE `accionesgrupales`
  MODIFY `id_formato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id_area` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `autoevaluaciones`
--
ALTER TABLE `autoevaluaciones`
  MODIFY `id_formato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
  MODIFY `id_carrera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `designaciones`
--
ALTER TABLE `designaciones`
  MODIFY `id_formato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `formatos`
--
ALTER TABLE `formatos`
  MODIFY `id_formato` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id_grupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `preguntasautoevaluacion`
--
ALTER TABLE `preguntasautoevaluacion`
  MODIFY `indice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT de la tabla `preguntasreporteindividual`
--
ALTER TABLE `preguntasreporteindividual`
  MODIFY `indice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `programavisitas`
--
ALTER TABLE `programavisitas`
  MODIFY `id_formato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `reportesindividuales`
--
ALTER TABLE `reportesindividuales`
  MODIFY `id_formato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tablaaccionesgrupales`
--
ALTER TABLE `tablaaccionesgrupales`
  MODIFY `indice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tablaprogramavisitas`
--
ALTER TABLE `tablaprogramavisitas`
  MODIFY `indice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT de la tabla `usuarios_carreras`
--
ALTER TABLE `usuarios_carreras`
  MODIFY `indice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT de la tabla `usuarios_grupos`
--
ALTER TABLE `usuarios_grupos`
  MODIFY `indice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
