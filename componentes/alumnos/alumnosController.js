app.controller('alumnosController', function($scope,$rootScope, $http, $filter, $interval, $route) {
  $scope.$on('$viewContentLoaded', function() {
    console.log('AlumnosController iniciado....');
    $scope.ordenado = 'matricula';
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.pages = [];
    $scope.formato = '';
    $scope.pestana = 'generales';
    $scope.alumnos = [];
    $scope.tutor = {};
    $scope.usuario = {};
    $scope.usuarioRespaldo = {};
    $scope.editando = false;
    $scope.rein = {
      id_formato: 0,
      formato: 'INSTRUMENTO DE AUTOEVALUACIÓN DIRIGIDO A TUTORES Y TUTORAS',
      estatus: 0,
      revision: 1,
      codigo: 'FOR-T-10-K',
      preguntas: [],
      fechaExpedicion: $filter('date')(new Date(), 'yyyy-MM-dd'),
      tutor:{},
      alumno:{}
    };
    $scope.declaraUsuario();
    $scope.obtieneDatos();
    $scope.obtieneAlumnos();
  });
  /*
  ██████   █████   ██████  ██ ███    ██  █████  ██████   ██████
  ██   ██ ██   ██ ██       ██ ████   ██ ██   ██ ██   ██ ██    ██
  ██████  ███████ ██   ███ ██ ██ ██  ██ ███████ ██   ██ ██    ██
  ██      ██   ██ ██    ██ ██ ██  ██ ██ ██   ██ ██   ██ ██    ██
  ██      ██   ██  ██████  ██ ██   ████ ██   ██ ██████   ██████
  */
  $scope.configPages = function() {
    $scope.pages.length = 0;
    var ini = $scope.currentPage - 4;
    var fin = $scope.currentPage + 5;
    if (ini < 1) {
      ini = 1;
      if (Math.ceil(_.size($scope.alumnos) / $scope.pageSize) > 8)
        fin = 8;
      else
        fin = Math.ceil(_.size($scope.alumnos) / $scope.pageSize);
    } else {
      if (ini >= Math.ceil(_.size($scope.alumnos) / $scope.pageSize) - 8) {
        ini = Math.ceil(_.size($scope.alumnos) / $scope.pageSize) - 8;
        fin = Math.ceil(_.size($scope.alumnos) / $scope.pageSize);
      }
    }
    if (ini < 1) ini = 1;
    for (var i = ini; i <= fin; i++) {
      $scope.pages.push({
        no: i
      });
    }

    if ($scope.currentPage >= $scope.pages.length)
      $scope.currentPage = $scope.pages.length - 1;
  };
  $scope.setPage = function(index) {
    $scope.currentPage = index - 1;
  };
  /*
  ███████ ██    ██ ███    ██  ██████ ██  ██████  ███    ██ ███████ ███████
  ██      ██    ██ ████   ██ ██      ██ ██    ██ ████   ██ ██      ██
  █████   ██    ██ ██ ██  ██ ██      ██ ██    ██ ██ ██  ██ █████   ███████
  ██      ██    ██ ██  ██ ██ ██      ██ ██    ██ ██  ██ ██ ██           ██
  ██       ██████  ██   ████  ██████ ██  ██████  ██   ████ ███████ ███████
  */
  $scope.declaraUsuario = function() {
    $scope.usuario = {
      id_usuario: 0,
      nombreCompleto: '',
      apmat: '',
      curp: '',
      tipo: 6,
      rein: ''
    };
  };
  $scope.resetea = () => {
    $route.reload();
  };
  /*
  ██ ███    ██ ██  ██████ ██  █████  ██      ██ ███████  █████  ██████
  ██ ████   ██ ██ ██      ██ ██   ██ ██      ██    ███  ██   ██ ██   ██
  ██ ██ ██  ██ ██ ██      ██ ███████ ██      ██   ███   ███████ ██████
  ██ ██  ██ ██ ██ ██      ██ ██   ██ ██      ██  ███    ██   ██ ██   ██
  ██ ██   ████ ██  ██████ ██ ██   ██ ███████ ██ ███████ ██   ██ ██   ██
  */

  $scope.iniciaFormato = function(formato,alumno) {
    switch (formato) {
      case 'rein':
        $scope.formato = 'rein';
        $scope.rein.alumno=alumno;
        $scope.rein.tutor = angular.copy($scope.tutor);
        console.log('Formato creado=>',$scope.rein);
        break;
    }
  };
  $scope.visualizaFormato = function(formato,alumno) {
    switch (formato) {
      case 'rein':
        $scope.formato = 'rein';
        $scope.rein = angular.copy(alumno.rein);
        $scope.rein.tutor = angular.copy($scope.tutor);
        $scope.rein.director = angular.copy($scope.director);
        $scope.rein.alumno = angular.copy(alumno);
        $scope.obtienePreguntasRein();
        break;
    }
  };
  $scope.guardaFormato = function(formato) {
    switch (formato) {
      case 'rein':
        $scope.setREIN();
        break;
    }
  };
  $scope.verComentarios = function(formato) {
    switch (formato) {
      case 'rein':
        alertify.alert('Comentarios', $scope.rein.comentarios);
        break;
    }
  };
  $scope.imprimir = (formato) => {
    switch (formato) {
      case 'rein':
        $scope.pdfRein();
        break;
    }
  };
  //
  // $interval(function() {
  //   $scope.obtieneUsuarios();
  //   $scope.obtieneCarreras();
  // }, 30000);
  /*
   ██████  ██████  ████████ ███████ ███    ██ ███████ ██████
  ██    ██ ██   ██    ██    ██      ████   ██ ██      ██   ██
  ██    ██ ██████     ██    █████   ██ ██  ██ █████   ██████
  ██    ██ ██   ██    ██    ██      ██  ██ ██ ██      ██   ██
   ██████  ██████     ██    ███████ ██   ████ ███████ ██   ██
  */
  $scope.obtieneDatos = function() {
    $http.post("./php/dbGrupos.php?action=getGrupoTutorado",{
      'usuario': $rootScope.main.usuario
    }).success(function(data) {
      $scope.tutor = angular.copy($rootScope.main.usuario);
      $scope.tutor.grupo = data[0];
      $scope.tutor.carrera = data[1];
      $scope.obtieneDirector();
      $scope.obtieneCoordinador();
    });
  };
  $scope.obtieneAlumnos = function() {
    $http.post("./php/dbTutor.php?action=getAlumnos",{
      'tutor': $rootScope.main.usuario
    }).success(function(data) {
      $scope.alumnos = data;
      $scope.configPages();
      // $scope.alumnos = _.filter($scope.alumnos,['id_grupo',$scope.tutor.grupo.id_grupo]);
      $scope.obtenerReportes();
    });
  };
  $scope.obtenerReportes = ()=>{
    angular.forEach($scope.alumnos,(alumno)=>{
      alumno.rein = {};
      $http.post("./php/dbAlumnos.php?action=getREIN",{
        'id': alumno.id_usuario
      }).success(function(data) {
        alumno.rein = data;
      });
    });
    console.log('Alumnos=>',$scope.alumnos);
  };
  $scope.obtienePreguntasRein = function() {
    $http.post("./php/dbAlumnos.php?action=getPreguntas", {
      'rein': $scope.rein
    }).success(function(data) {
      if (data.length) {
        $scope.rein.preguntas = data;
        console.log('Formato abierto=>',$scope.rein);
      } else {
        $scope.rein.preguntas = [];
      }
    });
  };
  $scope.obtieneCarreras = function() {
    $http.get("./php/dbCarreras.php?action=getCarreras")
      .success(function(data) {
        $scope.carreras = data;
      });
  };
  $scope.obtieneGrupos = function() {
    $http.get("./php/dbGrupos.php?action=getGrupos")
      .success(function(data) {
        $scope.grupos = data;
      });
  };
  $scope.obtieneDirector = () => {
    $http.post("./php/dbTutor.php?action=getDirector", {
      'usuario': $rootScope.main.usuario
    }).success(function(data) {
      $scope.director = data.nombreCompleto;
    });
  };
  $scope.obtieneCoordinador = () => {
    $http.get("./php/dbTutor.php?action=getCoordinador").success(function(data) {
      $scope.coordinador = data.nombreCompleto;
    });
  };
  /*
  ███████ ██████  ██ ████████  █████  ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  █████   ██   ██ ██    ██    ███████ ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  ███████ ██████  ██    ██    ██   ██ ██   ██
  */
  $scope.editarUsuario = function(p) {
    if (p) {
      $scope.editando = true;
      $scope.usuario = angular.copy(p);
      $scope.usuarioRespaldo = angular.copy($scope.usuario);
    } else {
      $scope.declaraUsuario();
      $scope.editando = false;
    }
  };
  /*
   ██████  ██    ██  █████  ██████  ██████   █████  ██████
  ██       ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
  ██   ███ ██    ██ ███████ ██████  ██   ██ ███████ ██████
  ██    ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
   ██████   ██████  ██   ██ ██   ██ ██████  ██   ██ ██   ██
  */
  $scope.guardarUsuario = function() {
    var nombreC = $scope.usuario.nombre + " " + $scope.usuario.appat + " " + $scope.usuario.apmat;
    $scope.usuario.nombreCompleto = nombreC;
    // console.log('vamo a guardar->',$scope.usuario);
    if ($scope.editando) {
      $http.post('./php/dbPit.php?action=setAlumno', {
        'usuario' : $scope.usuario
      }).success(function(data) {
        var idx = _.findIndex($scope.alumnos, $scope.usuarioRespaldo);
        if (idx > -1) {
          $scope.alumnos[idx] = $scope.usuario;
        }
        alertify.success('Alumno Modificado!');
      });
    } else {
      $http.post('./php/dbPit.php?action=setAlumno', {
        'usuario' : $scope.usuario,
        'tutor'   : $scope.tutor
      }).success(function(data) {
        if(data.success){
          $scope.alumnos.push($scope.usuario);
          alertify.success('Alumno Guardado!');
        }else{
          alertify.error('No se pudo guardar el alumno');
        }
      });
    }
    $('#modalUsuario').modal('hide');
  };
  /*
  ███████ ██      ██ ███    ███ ██ ███    ██  █████  ██████
  ██      ██      ██ ████  ████ ██ ████   ██ ██   ██ ██   ██
  █████   ██      ██ ██ ████ ██ ██ ██ ██  ██ ███████ ██████
  ██      ██      ██ ██  ██  ██ ██ ██  ██ ██ ██   ██ ██   ██
  ███████ ███████ ██ ██      ██ ██ ██   ████ ██   ██ ██   ██
  */
  $scope.eliminarUsuario = function(p) {
    $http.post("./php/dbPit.php?action=dropUsuario", {
      'id_usuario': p.id_usuario
    }).success(function(data) {
      var idx = _.findIndex($scope.alumnos, p);
      if (idx > -1) {
        $scope.alumnos.splice(idx, 1);
        alertify.success('Usuario Eliminado!');
      }
    });
  };
  /*
  ██████  ███████ ██ ███    ██
  ██   ██ ██      ██ ████   ██
  ██████  █████   ██ ██ ██  ██
  ██   ██ ██      ██ ██  ██ ██
  ██   ██ ███████ ██ ██   ████
  */
  $scope.setREIN = function() {
    $scope.rein.director = angular.copy($scope.director);
    $http.post("./php/dbAlumnos.php?action=setREIN", {
      'formato': $scope.rein
    }).success(function(data) {
      alertify.success('Documento Guardado Satisfactoriamente!');
      // console.log('data->',data);
      $route.reload();
    });
  };
  $scope.pdfRein = () => {
    var fechaExpedicion = moment($scope.rein.fechaExpedicion).locale('es').format("LL");
    var docDefinition = {
      pageMargins: [ 40, 120, 40, 40 ],
      pageSize: 'LETTER',
      header: function (currentPage, pageCount) {
          return {
            margin: [40,40],
            table: {
              height: 60,
              widths: [120, 280, 'auto'],
              body: [
                [{image:logo,width: 120,height: 60,rowSpan:3},{text: 'UNIVERSIDAD TECNOLÓGICA \nDE LOS VALLES CENTRALES DE OAXACA',style: 'cabecera',bold:false,rowSpan:2},{noWrap: true,text: [{text: 'Código:'},{text: $scope.rein.codigo,bold: true}]}],
                ['','',{noWrap: true,text: [{text: 'Revisión:'},{text: $scope.rein.revision,bold: true}]}],
                ['',{text: $scope.rein.formato ,style: 'cabecera'},{text:'Página '+currentPage.toString() + ' de '+pageCount,bold:true}]
              ]
            }
          };
      },
      content: [
        {
  		    style: 'tableExample',
      		color: '#444',
      		table: {
      		    widths: ['*','*','*'],
      // 			headerRows: 2,
      			// keepWithHeaderRows: 1,
      			body: [
      			    [{text:'Indicaciones: A continuación se presenta una serie de reactivos que usted necesita describir en caso de identificar a estudiantes altamente vulnerables en los ámbitos personal, académico,socioeconómico.',colSpan:3},{},{}],
      			    [{text: 'I. DATOS PERSONALES DEL TUTORADO', colSpan:3},{},{}],
      			    [{text: [{text:'Fecha: '},{text:$scope.rein.fechaExpedicion}], colSpan:3},{},{}],
      			    [{text: [{text:'Nombre: '},{text:$scope.rein.alumno.nombreCompleto}], colSpan:2},{},{text: [{text:'Edad: '},{text:$scope.rein.alumno.edad}]}],
      			    [{text: [{text:'Matrícula: '},{text:$scope.rein.alumno.matricula}]},{text: [{text:'Programa Educativo: '},{text:$scope.rein.alumno.id_carrera}], colSpan:2},{}],
      			    [{text: [{text:'Lugar de Nacimiento: '},{text:$scope.rein.alumno.lugarNacimiento}], colSpan:3},{},{}],
      			    [{text: [{text:'Municipio: '},{text:$scope.rein.alumno.municipio}]},{text: [{text:'Estado de residencia: '},{text:$scope.rein.alumno.estado}]},{text: [{text:'Lugar de residencia: '},{text:$scope.rein.alumno.lugarResidencia}]}],
      			    [{text: [{text:'Teléfono del tutorado: '},{text:$scope.rein.alumno.telefono}], colSpan:3},{},{}],
      			    [{text: [{text:'Nivel (TSU, Lic. O Ing): '},{text:$scope.rein.alumno.nivel}]},{text: [{text:'Cuatrimestre: '},{text:$scope.rein.alumno.cuatrimestre}]},{text: [{text:'Grupo: '},{text:$scope.rein.alumno.id_grupo}]}],
      			    [{text: [{text:'Actualmente con quien vive el tutorado: '},{text:$scope.rein.alumno.vive}], colSpan:3},{},{}],
      			    [{text: [
      			                {text:'II. DATOS FAMILIARES'},
      			                {text:'\n1. Nombre del padre: '},{text:$scope.rein.preguntas[0].respuesta},
      			                {text:', Vive: '},{text:$scope.rein.preguntas[1].respuesta},
      			                {text:', Edad: '},{text:$scope.rein.preguntas[2].respuesta},
      			                {text:', Ocupación: '},{text:$scope.rein.preguntas[3].respuesta},
      			                {text:'\n2. Nombre de la madre: '},{text:$scope.rein.preguntas[4].respuesta},
      			                {text:', Vive: '},{text:$scope.rein.preguntas[5].respuesta},
      			                {text:', Edad: '},{text:$scope.rein.preguntas[6].respuesta},
      			                {text:', Ocupación: '},{text:$scope.rein.preguntas[7].respuesta},
      			                {text:'\n3. N° de hijos (as): '},{text:$scope.rein.preguntas[8].respuesta},
      			                {text:'\n4. Ocupación de los padres: '},{text:$scope.rein.preguntas[9].respuesta},
      			                {text:'\n5. Dirección: '},{text:$scope.rein.preguntas[10].respuesta},
      			                {text:'\n6. Teléfono: '},{text:$scope.rein.preguntas[11].respuesta},
      			                {text:'\n7. Otras personas que vivan en casa: '},{text:$scope.rein.preguntas[12].respuesta},
      			                {text:'\n8. ¿Tiene hermanos? '},{text:$scope.rein.preguntas[14].respuesta},
      			                {text:'\n9. Grado de estudios de las y los hermanos: '},{text:$scope.rein.preguntas[16].respuesta},
      			            ], colSpan:3},{},{}],
      			    [{text: [{text:'10. Describe los datos significativos de la historia familiar del tutorado: '},{text:$scope.rein.preguntas[17].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'11. Describe los datos significativos de la historia escolar del tutorado: '},{text:$scope.rein.preguntas[18].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'12. Describe si el tutorado tiene participación en actividades (deportivas, culturales, sociales, de proyectos, etc.) en el lugar en donde vive: '},{text:$scope.rein.preguntas[19].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'13. Condiciones en las que vive el tutorado: '},{text:$scope.rein.preguntas[20].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'14. Indica las actividades que gustan y disgustan realizar en la escuela: '},{text:$scope.rein.preguntas[21].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'15. Indica las asignaturas que le gustan y disgustan realizar en la escuela: '},{text:$scope.rein.preguntas[22].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'16. Especifica el estado de ánimo actual del tutorado: '},{text:$scope.rein.preguntas[23].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'17. Especifica el rendimiento del tutorado en cada una de las asignaturas que cursa (nombre de la materia, promedio): '},{text:$scope.rein.preguntas[24].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'18. Registra la conducta, comportamiento y actitud del tutorado frente al estudio y sus tareas escolares: '},{text:$scope.rein.preguntas[25].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'19. Especifica ¿En cuántas escuelas ha estado el tutorado antes de la actual? '},{text:$scope.rein.preguntas[26].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'20. Describe el Historial médico vigente del tutorado: '},{text:$scope.rein.preguntas[27].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'21. Especifica la opinión general de los padres o responsables que conviven con el tutorado: '},{text:$scope.rein.preguntas[28].respuesta}], colSpan:3},{},{}],
      			    [{text: 'III. DESCRIPCIÓN DE LA SITUACIÓN ACTUAL', colSpan:3},{},{}],
                      [{text: [{text:'22. Descripción de la existencia de un problema familiar del entorno estudiantil que afecta al tutorado: '},{text:$scope.rein.preguntas[29].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'23. Describe ¿Cuál es la valoración que hace el tutorado sobre su problema? '},{text:$scope.rein.preguntas[30].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'24. Describe ¿Cuál es la actitud/ postura del tutorado ante su problema? '},{text:$scope.rein.preguntas[31].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'25. Describe el acuerdo con el tutorado sobre el/los apoyos pertinentes a sus necesidades: '},{text:$scope.rein.preguntas[32].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'26. Recomendaciones del tutor o tutora ante la problemática del tutorado: '},{text:$scope.rein.preguntas[33].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'27. Impresiones del tutor o tutora sobre el tutorado: '},{text:$scope.rein.preguntas[34].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'28. Describe la síntesis del problema del tutorado: '},{text:$scope.rein.preguntas[35].respuesta}], colSpan:3},{},{}],
      			    [{text: [{text:'29. Especifica el diagnóstico del problema del tutorado: '},{text:$scope.rein.preguntas[36].respuesta}], colSpan:3},{},{}],
      			    [{text: [
      			                {text:'30. Recomendaciones a atender sobre las siguientes situaciones:'},
      			                {text:[{text:'\na) Salud: '},{text:$scope.rein.preguntas[38].respuesta}]},
      			                {text:[{text:'\nb) Escolar: '},{text:$scope.rein.preguntas[39].respuesta}]},
      			                {text:[{text:'\nc) Comportamiento: '},{text:$scope.rein.preguntas[40].respuesta}]},
      			                {text:[{text:'\nd) Familia: '},{text:$scope.rein.preguntas[41].respuesta}]}
      			            ], colSpan:3},{},{}],
      			    [{text: [{text:'31. Acuerdos y compromisos del tutorado y tutor o tutora: '},{text:$scope.rein.preguntas[37].respuesta}], colSpan:3},{},{}],
      			]
      		}
      	}
      ],
      styles: misEstilos
    };
    pdfMake.createPdf(docDefinition).open();
  };
  /*
  ██████  ██████  ███████ ███████
  ██   ██ ██   ██ ██      ██
  ██████  ██   ██ █████   ███████
  ██      ██   ██ ██           ██
  ██      ██████  ██      ███████
  */
  var misEstilos = {
    cabecera: {
      fontSize: 11,
      bold: true,
      alignment: 'center'
    },
    subheader: {
      fontSize: 10,
      bold: false,
      fillColor: '#BDBDBD',
      color:'black',
      alignment: 'center'
    },
    headerIDAT: {
			bold: true,
			fontSize: 10,
      color:'black',
      alignment:'center'
		},
    numeros: {
		  alignment: 'center'
		},
    justificado: {
      fontSize: 10,
      alignment: 'justify',
      margin: [10, 5, 10, 1]
    },
    normal: {
      fontSize: 10,
      margin: [10, 5, 10, 1]
    },
    tableExample: {
      margin: [10, 5, 10, 5]
    },
    tableHeader: {
      bold: true,
      fontSize: 10,
      color: 'black'
    },
    defaultStyle: {
	    fontSize: 10,
	    color: 'black'
	 }
  };
  var logo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARkAAACaCAMAAACqncznAAABR1BMVEX///8Ak3iEwiUAkj+423ytra0AknW949y3rrAAjTvExMSzrq+NqKMAjDoAkXPF4IAXnYTe8e6fq6nS0tKPxzgXln9oo5g5qVL4+PgvmoYMl0Pw8PD39/f8/vpFno3z+eqo1Xd/xGne19nI5t683YNduqnl5eWQy26Dp6C4uLjq9vTIyMiKzcGWy0X6/faJxS7I5J6s1FZIrTqOxiOdzlGt3NTa7+vq9Nl5xrhGsZ0spo/O5IOk0l0wpU/c7sFaoZRnu2FntyrY7LrL5aOf1szB4JHj8c2r1Wak03VSs1plopdqwLDy5OeKyW2x2HDv9+P+7/KgwryJx0vMu7+71dB4tarT3duPs6ySxrysuLbGsLXHz87cys5Wq5u6xcNws6ZHrkt2wFZ0v0M1pSmq0kJXsi4gnjkzl0t5vzFeuXZywF+33qlvwHHWSAnhAAAabklEQVR4nO1d6V8aydYGgRZoFkV2FQGRTRCjgIIQcAcX1JhkMlkmc3MnGd+59////Napqm56qWpoIDHeeOY3EZuimnr61HO2qtJieZZneZZneZapZGvtZm/v7Oxs72Zt9bG/y88iqzcvt+2tcjkYrMzNVSrBcrVl3375y+Nzc3RanWNJpZo5Wnvsb/dosnbUCjJhoRI8/zXBeWk3hIWCkzl77O/5g2XriD2JGNJ6+StRzvHYuICUj2eCzeI8U9Yn6avI7suERFndnrXGA6Q6sGdOt08zg9bpDPhmtWllSmmSzjbYfY0vzaK+0zX7OKjYj88UaKzNAJkTzlecRB/npwXGytDUo/IYsJzNnlqKC+yvOD9BX1GO+o0v+7o+V0crTPX7mOp95jcUYktLS1mzfZWmxEUIfUC3VTHN3kjmrR7NDgyl1Dlf0uN0OMwiU+eo3/gSRnddUnb5cpQHE9zemiEaSmFTppDQfsVxpDElLkJMdDgKSpU5qowA5vxG+QVW914eb5+ebh8f7U2NF5syBWtAdBRWTPZVmxYYq0dUK+r2qJm0rWi8d3xeloGsBKunZ9Ogw6FM3wVSmbTJvorT0q/QQXfdNAFM8KXcdG2b4fEgizUxMmyLLYQcSKvN9sWmchPAhBxqRR0JzJ6MyymPjs4nxIZjsYX8BPS7PiUwVmterahHI4ApyxSzbeTwDG5Y33aU8Cw2mu5us31NTb9A+oq7no0gXxmYUZFD8Ng8MIscKwsWe9lkXxxPenxgQoj0FXe9GWGug0dnyAqBnFeDFWMUB6apmG2xfZNYbJ4nPT4yF6q7rhorQrA6yNglQa8GraoRktU9g6/OEI7FDk1isS+nBSbmULkypwbjrEBEPQSGoJOxK2y2HklzRMxmBmESiz19JKkm/TMDdWnpYJHAGVS52ATNaM0sLfbUroyafle5xqbSsjNhkeYVN8oqjx90chyziSz29JGkmn6PueMbGOCCsRkQvtnd1X60NeVohJiTb7Gj9UWG1BenjiQFdSS5xWPTFnse6dRmd+7rju7Dp2MCM4nFvlxgSqm4vl6/86gkz4PAwxQ1/XJUJshQGL8f/dNv95TYnD/8O+5yfdVpzZgszDYmhjE2h2YXFtF7UadK3ncETvdscShn8CqbLIIshsl1k/aI15XzK8BKIVxsrviDFprqWGk/ti9vbLE5NIuTf26HUpweKwsZHMJzRBFJsg2TDExyqCD+ns0b6XtdXQUw9j7CBYlrR6c0Y6W4JrDYHGJKpNNL2WW3SjZjbJWB7gtulmwqHkfGWGOSORmYXNfljbc1wHhtWFxxXR/lMXxhdioFuVsi12JziUnU2zKOQxBzjOVDsiZTZcgxPQkZf8rW7rf7cSUw/r7LRsV1qFOa0REUJy1jaLE5yT9tTgWEFyqM5xDssdy18yHH9K4pCEhhbO2IGpiUDAxrOlVH3nwCi80LJZAaOLRqYMDuY4TwRwxgWgryzaUwBjBt4jaXt61i5LgCmW86ZEaaJ/YzFYwsthkt47L7eCE8I2QKAjC9HiWXf/DPiNeW6iIblJJUJmMf+LtemwIZfUeZEfeewGLz6JelBhwQdXUBjgz0A8Ik07v228vVzEHqbz9MJW885Y8gDen6CTSD83LQ3nYZIxM05mBOIQVbbGZVmVcvYasBG0SfJpjmy7luPNUMmUV+e6WaSf39dxfNpVwkd5BDw3e52sSZac3Nvbi22YyRGWG4ORY7bMCPBkZeqwY8+h23gKX38yoDwi9df+bFq36/2+9HrpHz6/cjFy/SRjTct+OAqfJCOZmYPDNnN7o1h0uNLLaRkdeqgUEBS2vDmLKlQ4aoDCBjf/Ul5e97e71ut+dHRhvoN9W2eUFrguXBIOJSIhP5S49M1Wg6mbfYvHoJSw0M0mHjZVD1yBBXxp/q21NfWn7ksSAgcqlc1+aN9/z2g2QcXciUwRVsq5BJvWBMJ4M8DdsL80FWnPdMOYl0Fv3ybNi49MvIc5YJMNeRdtf+opUhyPiTba8XM4wfkBmUwRVMxpXI2Hp2hmfEd/aMaIDzTNmMzVYDnqckjp8O0zIw+DJ+NH8Qw3RfUWT8vTjQC1AvQsb7apfE4W3VZEKKpEdmwL0v5/l3DJ6pAWNrP8LL1JhZP6Cx2hXQmOtu7iAST/a+fElKyFCThKg3Ev/XwI8VSzWbrv0ZRpqd6wYbPH+eSeUFQSw14IDICiG4ovH0giiGTKFQyY+Q8b94dd3Ds8nes0seXnAnHm+nSKJmiIw34keg6acT16PhWI4LA/o1cJi1H+HYMEgt60IIrhxpHnLG3kvaCTLIaufsBBnZ831xaEOesC2CyFjhArsQjOhNfXKwwqFgbvTDpwGOw8yMJA3M3vjVCE1EKcWSWGeqX7rJiDJPlTmv/Inh8MbRHMsNgcFtWETD8fU42m5gsXlBEMujNWHDjKSqRaZHkeklqzv9ZMSWG4ZK1UqLOjEYDEo0LkQy+H0G0bCNkwFl8GiAU64eP5K0ju/KUFFlrsAaS8jk7C92Igd91zXlXnurUmnJtIsm0AFJQnhldtZHGqolN7JwKYP/1TlYMtXARAhhKKpsJyJgOxAwZWCMDB13NYirT7LjC6Tb9rpccTDnED347YzolFlCMG+xOVgy1YDnQrJCCENRRU5Be7LXTfUkBpaRQQQTxFU5ZRYvdZCLx/s9AKV3neqmWMiwEhHsfKWhxeaYMpYrM10ij2JCConKqko1mbS3U8g8aZAZBKskM6w0SG3kEoK+2FORuM3rjWQYyLBiSo6VMYix2abMp1toaNC7KfpdIySwpTApVdAWyE6pkMlkgmValRsaJCQ5cGv8ubYN6gfelJ+V69HflmNlIG/C++omIskpE3kUmRZRmiM1z2iReRUsl2lqAqAZer64ioBicJfk0oynM+YtNicIYnq0U0eSIKt0fZSCaRjI/DtYLQeHKXNFVhzFSpA4J797+0wG1vMML8fCd9x5vOQwEUmapN9ymdQRz/jI+B8Oc5rytqw0rrbfL+X1XHFIgeqR0dkm3o4UA4vNod88Qw2MquRmFla0JHdDDp7An1Eh89chdeNkQeGl7NL4peqBC/zBsfwZg5nB0XZOJJFgRZImilGGMpgL0nUu8pjOVch8PfxLFR3Q+WSjcLQPuhIw8JkMo6in9YENHBOOtrNzUGxKnTqRJ0lGZsg1yT61FMj4qw9f/H0dMlDkpzxDVMZFvWBGRKmLm9g21WdgsTluIcuj5cxUk/QLcjz86nsUmrISmfLcQ/DhMKiXh0MsD/+CwNsb7ycJeIxYW1OM41pskWexDT6hU7IZJPKovATOpQt9bwg0FYTJ9YHfD5krFCHuzu3q11PBGisiO7Z4PJKyHxyAH8yimaBmVZqJuJAIj7BZn+AUoybZCIQzEC26zuWGsEQr2e7mUv9027ZIZOcBBJSjUqk8yAKv4eLDw068HYG0aDd1ncslk3qdKatvyLHYBpUOMwkFNuw+0/RrkbxfyRnbgqh79yvmEJcXym5II2y2eLv/zz9/fosj7Yi34Z84upL6p98mDZF4vcA28fh/9FZb4wKzydTK908NFh7pPsFN5E2wrJiaJNnnOEIP/WGY9EYSj3Svr7tt/DrS9rbjrnbbBZDh65E4XJe9m1X9uja10eakEvgW2wylziaRJwndnyJDc5Op7H4jzr7XFUfTKplLteN48K4+ggRFjq4IzkMAaKleModUx+Ul4PzXcqNDRkXAvLQM32Ib7NT4Xok8Knt0AEMffm9wiOPDdvc66e8hbaHDdvURIICMzdaX0npepDlJf/K6DzrljZ/pS8HqZcEG351NkGYolbd5x7QrQ0QOswfDIZxFEI0k/RiW4VTpg09HkLF15ewVer+dSh74e6l++7/os9qNuyqa4Sx9AHVnP1RenZFJqbNK5EkipzrLQ70/sh8otYXk71IEH1x8i6faw1o/LN1DmnMwgA60RKPygHnb7pEGsFWG4+ozl1cZrcgzlciT5OVwFKfyKtVTpbYQYK7jriEyrvi1uqTtskVSmGuP1MBUlJOJt1lCCL9nqsw6x/dhUuosEnlqUW45qEr7JPfiyuUfMIlyJHB0UYpxtXsqaBB2f+LCkrZEo7wVd69aKPwazZxisbhYr6+vr8/P12onpX1eczalziKRpxFViVLaBvmfuGrU7SSdPS6ZfCM9FXqu+AP+6Jra11MGTQb7a4SFZrNJV8jzW9G2TFfGjNszruyphlI5fwkTYOu/ylG3k9KiX29Xyo+7+j3VSojDXaxwW6panGpB8LTbG6XRMil1ZpGkUrR+azkDB+38n0Id7HJSfIiMzZVSQvN1l+qHChll1mra3eXSYJmuDG9Fnjgp/WJhLK4PDo6PI3J2KjlMbyqQsdlywwwWLAXWI6OKJqc+3IMKy5WZVSJPK+wtlMFvUt5bUSzwdrvDJHC817MpthzokfkeKsOk1Fkl8rTC3pGxO/enCw9fubLKmxoiA9PsGkNDFo/reCao3Lk9G5VhU+rMEnk6YaRVMDY7sOJBuUwckYsCGWSz/NeoCdnZVNHZJqXKTL/xk4yWRamzS+TphLnzAKA5bF/7IyrjnEqpHMD+Qeob3ddENGRviIzKME196hQZbMIU/U6wQVUrvD3Ju5UXfbXPp0YG8Y68npOkqBQutdKX4Z26YxIYPD++YyJPL1v8gx4Ov9lUi/GUm1Nc8Z0HuSFxd4dxkyqWnPZEAjrYvIlIcmr6JcLfr727e/gtPoyhcteSDrlsCBdFhpikqOQAVUW/s5lMZCObNpKcaSKPIQYnrOzuPnz9RhJ8LowMfhH/9rWiypyT9XiyB6AupszC/8VaoJsfRpt6zCfyWMKxTxSbuYevO5AItuVyLvTj287hw5y6okC2k65JXK6pZk+PjIA0RmTYmtkm8liyNuIQvd3dXSgYBIOHDw8VRqGFTKYj+ltLs+122q33iGPCTpZdmnEijyk8061RHsZufpAKoRUag+mOPig2mVtgx4VFsCaguuAo6AbbxG9rxTdxIo8pL1kjHldIYYb6eYzjMuox+MajAOBIKOERRSYwtYVmcyGkk7H3do0pR4ZjNxS6HprYbOYRK5v5REg1eCY4jEHGOvmAE287d+unRxHJ7wGdOBhuzyNBQwkXVw4qL1l9Z51OhycfHkpeJYqTGVRDdMCJBGQ7/hJbCdIiQ2ZGvzI0Y3ANS4KKRW28Q3k2HaLIOYuBCnuIVNwcHVjmnGEwK/qVZOThlGyhzguoTJkDDNKaAu8khpFSWOIONLvElJnOJSyjDzRlCJ1LoDItgwN5otkld0En2mMZZJEHmZ31859M1hhL7UYILRBA8GUfeRJEdAUkKsn3Hs5MZdsk2UibSO1zlQlO0ntScmbucHYaOh7NtUyeFfcEZXV7fCKWiGWvytyU8j8nN+Oc0Q4iLRTYOv7fVxgqZ4w9kTqpyHryK/0pCMsZa/u1Ss5/GT3Ryhrnr/AQ+V4ntT8ROTtln85ZOT/6pSYQU/a2tedzBs9Pf9l5pJGtm7Pj00GrWq62zu3HZxOdGj2ZrNZqtUX6er1WU/1Fj3qtVrdYipqrfJkv7ZdOFke34wv6Bow/tvI4Em1YrY1lHERALnx/WVEOKFmtGyvRebmBsdRIKn1hf9TYFi+56G1YraWVnyWiKUH9DRcBoADsEZdUbyWc6XVYize6sFTCSVNImCWM4/DSgrWe5W5z9YWdsw/jJ5PFBasQvodXl3inpSLuNoPMPJSgwp58DMpWRtWW6ILV+ruTU6j6qZCxNGAx0TKu6wthpxsRS71OJoQWmcW6PFGixfqiynY2oZWzkM42oEiXVTdfRSqyWq/j17AS0iNGie9KWkBnRIlkZKL1uqxWUcV9f6hA4TewBE8d9nplMV0sNIB01cjgN/B1S3QfSt0Ll8MvjGei041Uod74lIeFAKQ5PgP1cqFZPEGfaJ5Ayh1noxctG+geDfxHtUhnG0A+FJkiXKLd1xvwNnz0hwt6iMLFPZTchJizsC/A2a2CNfRagwzwCLz1Dg8ANUD/N99KnZTwyRpkJl45N7PD5lFcJmzgTq13UVwL9vnuNpvW0ILg8+U3G+SGuDOKDL0EV+TlBp8fIb22QaYTnky/Q0nOkw8JQtitROYjVOQ6+Q4C4PdoHV0JBDwxwZqXGAU4yqlImuPmHtx8BRdQE3d5hGXifv0SDTXRCThApWKJmCOP2uQ94ZBg9SwRZK4A5nw+gZovQYn0IhAIgz7/eP6pwYqPdB2OAnG+XS/l399ffYYi9+2JAhk09M57J/wIf0QfiHkKt8XXnoBUW9mApgqaps33MbsD9E7nxzcC+pG2IGQC791uICbRWSj8Pr/vQXdsQJE0egnIvAUmen+fbiI0rpAd7wSWbtfvAjMtcI0nqwvo+9xfCmhssFAoffdmH33tjnilQGYTNCpwt4gvvcaqflkr3qazKzIyMafC4ENzz10RN1/eAJzd8ARi4qYFMzAm/gsnGiz6L/3bG9CksPMWkHl/h7Xo9SJqkb/fh3s1SuuWq/Qj2CxgGEcIVs8sIfJrLPhglmuQkaa74Is50phEwKlTdhESic6ACYoOm4fE9AbM06wKmSggk3fCZFy/bOI7ImSWMTLv5JoqkE4D+0jW5iR/zG5qQaQQuiAUCnQcSuQTAnrKWmRisVgCSQfRcf1zDDPsJ+k5ngARbJLOGiVcDpeai5sbMOosGD8tMujj6+j3UCcfE5TIhMhnY3nxylJLhDA4d7c/HhmyQhFhUcCk6xDvP+uQwQMpIFpwuwtL0WI0exeOKWgR/MUO9hc34I/jLauaq5BZBZ5xUmSWcUDQEZ33CUGeTa8X0EMCBircFwrZYnHlw2+dkNXXuZ/FOiaTQtbIQGSAmfPjSkM7m4BLY+KtZaPZqFlKTTyPXgMybkUXnbfR4iXecHpPm19Cc7BNCp1BFCTrzDJ+M//+ahlbxlvCwE3M28Vms1G0NJoLaB7dfrb6EuJsFjKZElj/CBuVlsENCV282xBglacKGVCK2DsEnPUuDb7hp3e1BnxbyWCsNiA8AOXzhRxiWmruQ82XNzTIoMmyrtAZFFW8Q6YJEfIVRuYKfJ5ECXUfW8qiz4Y+n5wsCELeObtlKeML+pq+Do4M0PPy+awNGB/yK3wYGR94evPIgsEKi7zoWD4J4dfIqxFlSx3doAsx0EUEF2nus4ZFEaYWYWBEx27QLp/vzRJ6HyODXDl8R0TtIrLRvgvUEj0AH+nJvbKBX/vQlJvt6osxZf4SOV84zC7uI/LLOzqfEoGP6/ufwuLS4uVGGFY+1C8Rp3Y8+AvWLxOoWdghKtcwzn+K4c+KeBkObQ678U4uEx4xa6nvf7pAHUVLzRj6/c1lIiBKDROeQOJTx5GuXX7Ko5aWGuoqcYHIZpm8Rt2K4mOoDJKrezddPrMMtfGr6McCQuPKXUCT+xaxKLyTdRecBbpmAr1GBKlZWJIuFJxO+aKiedqN0aId4TvcWj666UqmLFTfr8ibqCX+xBLuHuMeRa/l+z6uRLlGIKpMK60wmkXVeSdeFkp7B9Ydld1Hf5p01rM8y7M8y7M8y08rxVppf7+kzgCsztdq8hX0WiqeLdaG14s1XMVTSBT3pKrpQaMxi3w/mxQvaZ6lWVJcxalcaUQQSTbwKxyx1+hlOBGgqfhMtER7aihQhs8uPMnqPaROrHQ5emNYJ4C9VHJmBk7rEd6AHwbprNgSWT5LDpF4Jye1i41hT3IVAKe3hDc/g29rUiBtL1hjCcgcIWgkl3QRJ3NCAeqV4hLcHzTJj6Iq3AxvnRESH6WuGqSnWAigeU0vYt0TYjNejP4jBOcVPKLT6cjDiD5TKEgyRwgPywYIgyvcuiOd5kJSYVYPTcOVcE9OpzMQExAWWbl/nBb6OVYjm5B5egzjZjZ7dUcVw0JKmejZD581njm/1cgGQhIPQ00lhjStQ9DD2+o80NPVB/hkAOsV6B7kTBOPkJCaTvbJjmzygDdgf+AVvALA8vA3KMmvFjIrQiErHEVD81iXgClSjxD5fZ7socM91T7nA2RnFOieJyYM5+WTkQY+i54+UFCJDtYSSLI5PCoWIdtb5cHjqmfi/RC9fbztkqZXok7RDS+x7jnDgmJePhFBUwBNDymZWbfSdQFo1L6Es9BQcDA5TQ7mEk3wnQAS7+8AH3xlA08miU7oCp15vNf3j4Wnx8Hw4GNy2RHndSHbVsLPH1LBimddwpTslBJ8GDYowCFA4BJ+oa0wbuDD7LLg+OQfoV4yheDKk5xhxMjALmSsSc7bKPwuHX1AdAYDBwL61XnvJjMQ0MPIuPXdQ010/elxMIw9JO8iXcdDT5Ofnt/W62ByJQ6mPHOhMOsXnrv6PNTYQFUwfd+ruwfd65BGT46DG3ghAP3lBNdz06pzwKRnLR0CIJl15TES2KBhK0eRmS+RcEq5P973xDgYxtOR7E8D00sWHPrhDlbyrMGfEcId2YWrWeU2VsLB2GpTnmmQQGNe1eiJcTDew/4beV0i54xEietCROJg8IFjzkAI7C+MvjFsYyUcjGNNQrOASOjtCtY9RaMnxsFAD6F3xejq4j6xPW5sdTwOvGkXXBrg4Bo5cOUeg3ZH7buDbOxFiuQDDj4hPVnwmjQUQ2xieqe7f8GlSTxSLWlCIYfeLjTwD19CdKQXsXqIePdlmigCeP4kXgIg0XwirnMhDU3+AEQkDqY94eLuCVlggBt9WJDm5dORYkP6e+sCrJAo4IkTdir8ucTHfawiYMPg/DTh8wqMPiDKS7HQB65IaZd2FILiLmZ3aXnrpfXp+cErb2JkXU8sL4qFlSh+utKogXlDF9ZhvAQLAUKfwU8RJU8HploC0+s70lOog2LUK+JRi9QxXLcqfKMnI5uBfDiMYkAnPhqhuIh4QR5D8QOiiQ/oklSLXwTeePs2oDhMA45OIEs+l6AnT8ApFrKW1eLbgGKxAG705BJYy2m3E42G7lxfdrsV9eWVTbc7nUaXJCCy8BouDFMuWfkT0JOIoskV9WXptyc2nYjMbNf3E9s+/izP8ixD+X/XbryS5pipDwAAAABJRU5ErkJggg==';
  $scope.inputType = 'password';
    $scope.showHideClass = 'fas fa-eye';

    $scope.showPassword = function(){
     if($scope.usuario.password != null)
     {
      if($scope.inputType == 'password')
      {
       $scope.inputType = 'text';
       $scope.showHideClass = 'fas fa-eye-slash';
      }
      else
      {
       $scope.inputType = 'password';
       $scope.showHideClass = 'fas fa-eye';
      }
     }
    };
});
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}
function Numeros(string){//Solo numeros
    var out = '';
    var filtro = '1234567890';//Caracteres validos

    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos
    for (var i=0; i<string.length; i++)
       if (filtro.indexOf(string.charAt(i)) != -1)
             //Se añaden a la salida los caracteres validos
	     out += string.charAt(i);

    //Retornar valor filtrado
    return out;
}
function mayus(e) {
    e.value = e.value.toUpperCase();
}
