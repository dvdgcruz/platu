app.controller('usuariosController', function($scope, $http, $filter, $interval) {
  $scope.$on('$viewContentLoaded', function() {
    console.log('UsuariosController iniciado....');
    $scope.ordenado = 'matricula';
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.pages = [];
    $scope.usuarios = [];
    $scope.carreras = [];
    $scope.carrerasSeleccionadas = [];
    $scope.usuario = {};
    $scope.carrera = {};
    $scope.usuarioRespaldo = {};
    $scope.editando = false;
    $scope.pestana = 'generales';
    $scope.declaraUsuario();
    $scope.obtieneUsuarios();
    $scope.obtieneCarreras();
  });
  /*
  ██████   █████   ██████  ██ ███    ██  █████  ██████   ██████
  ██   ██ ██   ██ ██       ██ ████   ██ ██   ██ ██   ██ ██    ██
  ██████  ███████ ██   ███ ██ ██ ██  ██ ███████ ██   ██ ██    ██
  ██      ██   ██ ██    ██ ██ ██  ██ ██ ██   ██ ██   ██ ██    ██
  ██      ██   ██  ██████  ██ ██   ████ ██   ██ ██████   ██████
  */
  $scope.configPages = function() {
    $scope.pages.length = 0;
    var ini = $scope.currentPage - 4;
    var fin = $scope.currentPage + 5;
    if (ini < 1) {
      ini = 1;
      if (Math.ceil(_.size($scope.usuarios) / $scope.pageSize) > 8)
        fin = 8;
      else
        fin = Math.ceil(_.size($scope.usuarios) / $scope.pageSize);
    } else {
      if (ini >= Math.ceil(_.size($scope.usuarios) / $scope.pageSize) - 8) {
        ini = Math.ceil(_.size($scope.usuarios) / $scope.pageSize) - 8;
        fin = Math.ceil(_.size($scope.usuarios) / $scope.pageSize);
      }
    }
    if (ini < 1) ini = 1;
    for (var i = ini; i <= fin; i++) {
      $scope.pages.push({
        no: i
      });
    }

    if ($scope.currentPage >= $scope.pages.length)
      $scope.currentPage = $scope.pages.length - 1;
  };
  $scope.setPage = function(index) {
    $scope.currentPage = index - 1;
  };
  /*
  ███████ ██    ██ ███    ██  ██████ ██  ██████  ███    ██ ███████ ███████
  ██      ██    ██ ████   ██ ██      ██ ██    ██ ████   ██ ██      ██
  █████   ██    ██ ██ ██  ██ ██      ██ ██    ██ ██ ██  ██ █████   ███████
  ██      ██    ██ ██  ██ ██ ██      ██ ██    ██ ██  ██ ██ ██           ██
  ██       ██████  ██   ████  ██████ ██  ██████  ██   ████ ███████ ███████
  */
  $scope.declaraUsuario = function() {
    $scope.usuario = {
      id_usuario: 0,
      nombreCompleto: '',
      apmat: '',
      curp: '',
      correo: '',
      telefono: '',
      nss: '',
      fechaCreacion: $filter('date')(new Date(), 'yyyy-MM-dd'),
      carreras: [],
      tipo: 0,
      abreviatura: '',
      tiempoCompleto: 1,
    };
  };

  $interval(function() {
    if (!$scope.editando) {
      $scope.obtieneUsuarios();
      $scope.obtieneCarreras();
    }
  }, 10000);
  /*
   ██████  ██████  ████████ ███████ ███    ██ ███████ ██████
  ██    ██ ██   ██    ██    ██      ████   ██ ██      ██   ██
  ██    ██ ██████     ██    █████   ██ ██  ██ █████   ██████
  ██    ██ ██   ██    ██    ██      ██  ██ ██ ██      ██   ██
   ██████  ██████     ██    ███████ ██   ████ ███████ ██   ██
  */
  $scope.obtieneUsuarios = function() {
    $http.get("./php/dbPit.php?action=getUsuarios")
      .success(function(data) {
        $scope.usuarios = data;
        $scope.configPages();
      });
  };
  $scope.obtieneCarreras = function() {
    $http.get("./php/dbCarreras.php?action=getCarreras")
      .success(function(data) {
        $scope.carreras = data;
        $scope.carrerasSeleccionadas = angular.copy(data);
      });
  };
  $scope.obtieneCarrerasPorUsuario = function(itm) {
    $scope.carrerasSeleccionadas = angular.copy($scope.carreras);
    $http.post("./php/dbCarreras.php?action=getCarrerasPorUsuario", {
      'usuario': itm
    }).success(function(data) {
      angular.forEach(data, (car) => {
        $scope.carrerasSeleccionadas[_.findIndex($scope.carrerasSeleccionadas, {
          'id_carrera': car.id_carrera
        })].selected = true;
        car.selected = true;
      });
      itm.carreras = data;
    });
  };
  /*
  ███████ ██████  ██ ████████  █████  ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  █████   ██   ██ ██    ██    ███████ ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  ███████ ██████  ██    ██    ██   ██ ██   ██
  */
  $scope.editarUsuario = function(p) {
    if (p) {
      $scope.editando = true;
      $scope.usuario = angular.copy(p);
      $scope.obtieneCarrerasPorUsuario($scope.usuario);
      $scope.usuarioRespaldo = angular.copy($scope.usuario);
      console.log('usuario actual->', $scope.usuario);
    } else {
      $scope.declaraUsuario();
      $scope.editando = false;
    }
  };
  /*
   ██████  ██    ██  █████  ██████  ██████   █████  ██████
  ██       ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
  ██   ███ ██    ██ ███████ ██████  ██   ██ ███████ ██████
  ██    ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
   ██████   ██████  ██   ██ ██   ██ ██████  ██   ██ ██   ██
  */
  $scope.guardarUsuario = function() {
    angular.forEach($scope.carrerasSeleccionadas, (itm) => {
      if (itm.selected) {
        $scope.usuario.carreras.push(itm);
      }
    });
    var nombreC = $scope.usuario.abreviatura + " " + $scope.usuario.nombre + " " + $scope.usuario.appat + " " + $scope.usuario.apmat;
    $scope.usuario.nombreCompleto = nombreC;
    if ($scope.editando) {
      $http.post('./php/dbPit.php?action=setUsuario', {
        'usuario': $scope.usuario
      }).success(function(data) {
        var idx = _.findIndex($scope.usuarios, $scope.usuarioRespaldo);
        if (idx > -1) {
          $scope.usuarios[idx] = $scope.usuario;
        }
        alertify.success('Usuario Modificado!');
      });
    } else {
      $http.post('./php/dbPit.php?action=setUsuario', {
        'usuario': $scope.usuario
      }).success(function(data) {
        $scope.usuarios.push($scope.usuario);
        alertify.success('Usuario Guardado!');
      });
    }
    $('#modalUsuario').modal('hide');
  };
  /*
  ███████ ██      ██ ███    ███ ██ ███    ██  █████  ██████
  ██      ██      ██ ████  ████ ██ ████   ██ ██   ██ ██   ██
  █████   ██      ██ ██ ████ ██ ██ ██ ██  ██ ███████ ██████
  ██      ██      ██ ██  ██  ██ ██ ██  ██ ██ ██   ██ ██   ██
  ███████ ███████ ██ ██      ██ ██ ██   ████ ██   ██ ██   ██
  */
  $scope.eliminarUsuario = function(p) {
    $http.post("./php/dbPit.php?action=dropUsuario", {
      'id_usuario': p.id_usuario
    }).success(function(data) {
      var idx = _.findIndex($scope.usuarios, p);
      if (idx > -1) {
        $scope.usuarios.splice(idx, 1);
        alertify.success('Usuario Eliminado!');
      }
    });
  };
  $scope.inputType = 'password';
    $scope.showHideClass = 'fas fa-eye';

    $scope.showPassword = function(){
     if($scope.usuario.password != null)
     {
      if($scope.inputType == 'password')
      {
       $scope.inputType = 'text';
       $scope.showHideClass = 'fas fa-eye-slash';
      }
      else
      {
       $scope.inputType = 'password';
       $scope.showHideClass = 'fas fa-eye';
      }
     }
    };
});
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = [8, 37, 39, 46];

    tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}
function Numeros(string){//Solo numeros
    var out = '';
    var filtro = '1234567890';//Caracteres validos

    //Recorrer el texto y verificar si el caracter se encuentra en la lista de validos
    for (var i=0; i<string.length; i++)
       if (filtro.indexOf(string.charAt(i)) != -1)
             //Se añaden a la salida los caracteres validos
	     out += string.charAt(i);

    //Retornar valor filtrado
    return out;
}
function mayus(e) {
    e.value = e.value.toUpperCase();
}
