app.controller('loginController', ['$scope', '$http', '$rootScope', '$location', function($scope, $http, $rootScope, $location) {
  $scope.$on('$viewContentLoaded', function() {
    console.log('LoginController iniciado....');
    $scope.usuario = {};
  });
  $scope.validaLogin = function() {
    $http.post('./php/dbPit.php?action=validaLogin', {
      'usuario': $scope.usuario
    }).success(function(data) {
      if (Object.keys(data).length > 0) {
        alertify.success('Bienvenido!');
        $rootScope.guardaUsuario(data);
      } else {
        alertify.error('Usuario NO encontrado!');
      }
    });
  };
}]);
function mayus(e) {
    e.value = e.value.toUpperCase();
}
