app.controller("dashboardController", function($scope,$rootScope) {
  $scope.$on('$viewContentLoaded', function() {
    console.log('DashboardController iniciado....');
    if (localStorage.usuario) {
      var usuario = JSON.parse(localStorage.usuario);
      $rootScope.main.usuario = usuario;
    }
  });
});
