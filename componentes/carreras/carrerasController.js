app.controller('carrerasController', function($scope, $http, $filter, $interval, $route) {
  $scope.$on('$viewContentLoaded', function() {
    console.log('CarrerasController iniciado....');
    $scope.ordenado = 'carrera';
    $scope.cpCar = 0;
    $scope.psCar = 8;
    $scope.pCar = [];
    $scope.carreras = [];
    $scope.carrera = {};
    $scope.carreraRespaldo = {};
    $scope.editandoCarrera = false;
    $scope.obtieneCarreras();
  });
  /*
  ██████   █████   ██████  ██ ███    ██  █████   ██████ ██  ██████  ███    ██
  ██   ██ ██   ██ ██       ██ ████   ██ ██   ██ ██      ██ ██    ██ ████   ██
  ██████  ███████ ██   ███ ██ ██ ██  ██ ███████ ██      ██ ██    ██ ██ ██  ██
  ██      ██   ██ ██    ██ ██ ██  ██ ██ ██   ██ ██      ██ ██    ██ ██  ██ ██
  ██      ██   ██  ██████  ██ ██   ████ ██   ██  ██████ ██  ██████  ██   ████
  */
  $scope.configPagesCar = function() {
    $scope.pCar.length = 0;
    var ini = $scope.cpCar - 4;
    var fin = $scope.cpCar + 5;
    if (ini < 1) {
      ini = 1;
      if (Math.ceil(_.size($scope.carreras) / $scope.psCar) > 8)
        fin = 8;
      else
        fin = Math.ceil(_.size($scope.carreras) / $scope.psCar);
    } else {
      if (ini >= Math.ceil(_.size($scope.carreras) / $scope.psCar) - 8) {
        ini = Math.ceil(_.size($scope.carreras) / $scope.psCar) - 8;
        fin = Math.ceil(_.size($scope.carreras) / $scope.psCar);
      }
    }
    if (ini < 1) ini = 1;
    for (var i = ini; i <= fin; i++) {
      $scope.pCar.push({
        no: i
      });
    }

    if ($scope.cpCar >= $scope.pCar.length)
      $scope.cpCar = $scope.pCar.length - 1;
  };
  $scope.setPageCar = function(index) {
    $scope.cpCar = index - 1;
  };
  /*
  ███████ ██    ██ ███    ██  ██████ ██  ██████  ███    ██ ███████ ███████
  ██      ██    ██ ████   ██ ██      ██ ██    ██ ████   ██ ██      ██
  █████   ██    ██ ██ ██  ██ ██      ██ ██    ██ ██ ██  ██ █████   ███████
  ██      ██    ██ ██  ██ ██ ██      ██ ██    ██ ██  ██ ██ ██           ██
  ██       ██████  ██   ████  ██████ ██  ██████  ██   ████ ███████ ███████
  */
  $scope.declaraCarrera = function() {
    $scope.carrera = {
      id_carrera: 0,
      carrera: ''
    };
  };
  $interval(function() {
    $scope.obtieneCarreras();
  }, 10000);
  /*
   ██████  ██████  ████████ ███████ ███    ██ ███████ ██████
  ██    ██ ██   ██    ██    ██      ████   ██ ██      ██   ██
  ██    ██ ██████     ██    █████   ██ ██  ██ █████   ██████
  ██    ██ ██   ██    ██    ██      ██  ██ ██ ██      ██   ██
   ██████  ██████     ██    ███████ ██   ████ ███████ ██   ██
  */
  $scope.obtieneCarreras = function() {
    $http.get("./php/dbCarreras.php?action=getCarreras")
      .success(function(data) {
        $scope.carreras = data;
        $scope.configPagesCar();
      });
  };
  /*
  ███████ ██████  ██ ████████  █████  ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  █████   ██   ██ ██    ██    ███████ ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  ███████ ██████  ██    ██    ██   ██ ██   ██
  */
  $scope.editarCarrera = function(p) {
    if (p) {
      $scope.editandoCarrera = true;
      $scope.carrera = angular.copy(p);
      $scope.carreraRespaldo = angular.copy($scope.carrera);
    } else {
      $scope.declaraCarrera();
      $scope.editandoCarrera = false;
    }
  };
  /*
   ██████  ██    ██  █████  ██████  ██████   █████  ██████
  ██       ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
  ██   ███ ██    ██ ███████ ██████  ██   ██ ███████ ██████
  ██    ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
   ██████   ██████  ██   ██ ██   ██ ██████  ██   ██ ██   ██
  */
  $scope.guardarCarrera = function() {
    if ($scope.editandoCarrera) {
      $http.post('./php/dbCarreras.php?action=setCarrera', {
        'carrera': $scope.carrera
      }).success(function(data) {
        var idx = _.findIndex($scope.carreras, $scope.carreraRespaldo);
        if (idx > -1) {
          $scope.carreras[idx] = $scope.carrera;
        }
        alertify.success('Carrera Modificada!');
      }).error(function(data) {});
    } else {
      $http.post('./php/dbCarreras.php?action=setCarrera', {
        'carrera': $scope.carrera
      }).success(function(data) {
        $scope.carrera.id_carrera = data.success;
        $scope.carreras.push($scope.carrera);
        alertify.success('Carrera Guardada!');
      });
    }
    $('#modalCarrera').modal('hide');
  };
  /*
  ███████ ██      ██ ███    ███ ██ ███    ██  █████  ██████
  ██      ██      ██ ████  ████ ██ ████   ██ ██   ██ ██   ██
  █████   ██      ██ ██ ████ ██ ██ ██ ██  ██ ███████ ██████
  ██      ██      ██ ██  ██  ██ ██ ██  ██ ██ ██   ██ ██   ██
  ███████ ███████ ██ ██      ██ ██ ██   ████ ██   ██ ██   ██
  */
  $scope.eliminarCarrera = function(p) {
    $http.post("./php/dbCarreras.php?action=dropCarrera", {
      'id_carrera': p.id_carrera
    }).success(function(data) {
      var idx = _.findIndex($scope.carreras, p);
      if (idx > -1) {
        $scope.carreras.splice(idx, 1);
        alertify.success('Carrera Eliminada!');
      }
    });
  };
});

function soloLetras(e) {
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
  especiales = [8, 37, 39, 46];

  tecla_especial = false
  for(var i in especiales) {
      if(key == especiales[i]) {
          tecla_especial = true;
          break;
      }
  }

  if(letras.indexOf(tecla) == -1 && !tecla_especial)
      return false;
}
