app.controller('gruposController', function($scope, $http, $filter, $interval, $route) {
  $scope.$on('$viewContentLoaded', function() {
    console.log('GruposController iniciado....');
    $scope.ordenado = 'carrera';
    $scope.cpGpo = 0;
    $scope.psGpo = 8;
    $scope.pGpo = [];
    $scope.carreras = [];
    $scope.grupos = [];
    $scope.areas = [];
    $scope.grupo = {};
    $scope.grupoRespaldo = {};
    $scope.editandoGrupo = false;
    $scope.obtieneCarreras();
    $scope.obtieneAreas();
    $scope.obtieneGrupos();

  });
  /*
  ██████   █████   ██████  ██ ███    ██  █████   ██████ ██  ██████  ███    ██
  ██   ██ ██   ██ ██       ██ ████   ██ ██   ██ ██      ██ ██    ██ ████   ██
  ██████  ███████ ██   ███ ██ ██ ██  ██ ███████ ██      ██ ██    ██ ██ ██  ██
  ██      ██   ██ ██    ██ ██ ██  ██ ██ ██   ██ ██      ██ ██    ██ ██  ██ ██
  ██      ██   ██  ██████  ██ ██   ████ ██   ██  ██████ ██  ██████  ██   ████
  */
  $scope.configPagesGpo = function() {
    $scope.pGpo.length = 0;
    var ini = $scope.cpGpo - 4;
    var fin = $scope.cpGpo + 5;
    if (ini < 1) {
      ini = 1;
      if (Math.ceil(_.size($scope.grupos) / $scope.psGpo) > 8)
        fin = 8;
      else
        fin = Math.ceil(_.size($scope.grupos) / $scope.psGpo);
    } else {
      if (ini >= Math.ceil(_.size($scope.grupos) / $scope.psGpo) - 8) {
        ini = Math.ceil(_.size($scope.grupos) / $scope.psGpo) - 8;
        fin = Math.ceil(_.size($scope.grupos) / $scope.psGpo);
      }
    }
    if (ini < 1) ini = 1;
    for (var i = ini; i <= fin; i++) {
      $scope.pGpo.push({
        no: i
      });
    }

    if ($scope.cpGpo >= $scope.pGpo.length)
      $scope.cpGpo = $scope.pGpo.length - 1;
  };
  $scope.setPageGpo = function(index) {
    $scope.cpGpo = index - 1;
  };
  /*
  ███████ ██    ██ ███    ██  ██████ ██  ██████  ███    ██ ███████ ███████
  ██      ██    ██ ████   ██ ██      ██ ██    ██ ████   ██ ██      ██
  █████   ██    ██ ██ ██  ██ ██      ██ ██    ██ ██ ██  ██ █████   ███████
  ██      ██    ██ ██  ██ ██ ██      ██ ██    ██ ██  ██ ██ ██           ██
  ██       ██████  ██   ████  ██████ ██  ██████  ██   ████ ███████ ███████
  */
  $scope.declaraGrupo = function() {
    $scope.grupo = {
      id_grupo: 0,
      grupo: '',
      carrera: {},
      area: {}
    };
  };
  $interval(function() {
    $scope.obtieneCarreras();
    $scope.obtieneAreas();
    $scope.obtieneGrupos();
  }, 10000);
  /*
   ██████  ██████  ████████ ███████ ███    ██ ███████ ██████
  ██    ██ ██   ██    ██    ██      ████   ██ ██      ██   ██
  ██    ██ ██████     ██    █████   ██ ██  ██ █████   ██████
  ██    ██ ██   ██    ██    ██      ██  ██ ██ ██      ██   ██
   ██████  ██████     ██    ███████ ██   ████ ███████ ██   ██
  */
  $scope.obtieneCarreras = function() {
    $http.get("./php/dbCarreras.php?action=getCarreras")
      .success(function(data) {
        $scope.carreras = data;
      });
  };
  $scope.obtieneAreas = function() {
    $http.get("./php/dbAreas.php?action=getAreas")
      .success(function(data) {
        $scope.areas = data;
      });
  };
  $scope.obtieneGrupos = function() {
    $http.get("./php/dbGrupos.php?action=getGrupos")
      .success(function(data) {
        $scope.grupos = data;
        $scope.configPagesGpo();
      });
  };
  /*
  ███████ ██████  ██ ████████  █████  ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  █████   ██   ██ ██    ██    ███████ ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  ███████ ██████  ██    ██    ██   ██ ██   ██
  */
  $scope.editarGrupo = function(p) {
    $scope.declaraGrupo();
    if (p) {
      $scope.editandoGrupo = true;
      $scope.grupo = angular.copy(p);
      $scope.grupoRespaldo = angular.copy($scope.grupo);
      $scope.grupo.carrera = angular.copy(_.find($scope.carreras, ['id_carrera', p.id_carrera]));
      $scope.grupo.area = angular.copy(_.find($scope.areas, ['id_area', p.id_area]));
    } else {
      $scope.editandoGrupo = false;
    }
  };
  /*
   ██████  ██    ██  █████  ██████  ██████   █████  ██████
  ██       ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
  ██   ███ ██    ██ ███████ ██████  ██   ██ ███████ ██████
  ██    ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
   ██████   ██████  ██   ██ ██   ██ ██████  ██   ██ ██   ██
  */
  $scope.guardarGrupo = function() {
    if ($scope.editandoGrupo) {
      $http.post('./php/dbGrupos.php?action=setGrupo', {
        'grupo': $scope.grupo
      }).success(function(data) {
        var idx = _.findIndex($scope.grupos, $scope.grupoRespaldo);
        if (idx > -1) {
          if ($scope.grupo.carrera) {
            $scope.grupo.id_carrera = angular.copy($scope.grupo.carrera.id_carrera);
          }
          if ($scope.grupo.area) {
            $scope.grupo.id_area = angular.copy($scope.grupo.area.id_area);
          }
          $scope.grupos[idx] = $scope.grupo;
        }
        alertify.success('Grupo Modificado!');
      }).error(function(data) {});
    } else {
      $http.post('./php/dbGrupos.php?action=setGrupo', {
        'grupo': $scope.grupo
      }).success(function(data) {
        $scope.grupo.id_grupo = data.success;
        if ($scope.grupo.carrera) {
          $scope.grupo.id_carrera = angular.copy($scope.grupo.carrera.id_carrera);
        }
        if ($scope.grupo.area) {
          $scope.grupo.id_area = angular.copy($scope.grupo.area.id_area);
        }
        $scope.grupos.push($scope.grupo);
        alertify.success('Grupo Guardado!');
      });
    }
    $('#modalGrupo').modal('hide');
  };
  /*
  ███████ ██      ██ ███    ███ ██ ███    ██  █████  ██████
  ██      ██      ██ ████  ████ ██ ████   ██ ██   ██ ██   ██
  █████   ██      ██ ██ ████ ██ ██ ██ ██  ██ ███████ ██████
  ██      ██      ██ ██  ██  ██ ██ ██  ██ ██ ██   ██ ██   ██
  ███████ ███████ ██ ██      ██ ██ ██   ████ ██   ██ ██   ██
  */
  $scope.eliminarGrupo = function(p) {
    $http.post("./php/dbGrupos.php?action=dropGrupo", {
      'id_grupo': p.id_grupo
    }).success(function(data) {
      var idx = _.findIndex($scope.grupos, p);
      if (idx > -1) {
        $scope.grupos.splice(idx, 1);
        alertify.success('Grupo Eliminado!');
      }
    });
  };
});

function soloLetras(e) {
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
  especiales = [8, 37, 39, 46];

  tecla_especial = false
  for(var i in especiales) {
      if(key == especiales[i]) {
          tecla_especial = true;
          break;
      }
  }

  if(letras.indexOf(tecla) == -1 && !tecla_especial)
      return false;
}
