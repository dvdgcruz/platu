app.controller('areasController', function($scope, $http, $filter, $interval, $route) {
  $scope.$on('$viewContentLoaded', function() {
    console.log('AreasController iniciado....');
    $scope.ordenado = 'carrera';
    $scope.cpCar = 0;
    $scope.psCar = 8;
    $scope.pCar = [];
    $scope.carreras = [];
    $scope.areas = [];
    $scope.area = {};
    $scope.areaRespaldo = {};
    $scope.editandoArea = false;
    $scope.obtieneCarreras();
    $scope.obtieneAreas();
  });
  /*
  ██████   █████   ██████  ██ ███    ██  █████   ██████ ██  ██████  ███    ██
  ██   ██ ██   ██ ██       ██ ████   ██ ██   ██ ██      ██ ██    ██ ████   ██
  ██████  ███████ ██   ███ ██ ██ ██  ██ ███████ ██      ██ ██    ██ ██ ██  ██
  ██      ██   ██ ██    ██ ██ ██  ██ ██ ██   ██ ██      ██ ██    ██ ██  ██ ██
  ██      ██   ██  ██████  ██ ██   ████ ██   ██  ██████ ██  ██████  ██   ████
  */

  $scope.configPagesCar = function() {
    $scope.pCar.length = 0;
    var ini = $scope.cpCar - 4;
    var fin = $scope.cpCar + 5;
    if (ini < 1) {
      ini = 1;
      if (Math.ceil(_.size($scope.areas) / $scope.psCar) > 8)
        fin = 8;
      else
        fin = Math.ceil(_.size($scope.areas) / $scope.psCar);
    } else {
      if (ini >= Math.ceil(_.size($scope.areas) / $scope.psCar) - 8) {
        ini = Math.ceil(_.size($scope.areas) / $scope.psCar) - 8;
        fin = Math.ceil(_.size($scope.areas) / $scope.psCar);
      }
    }
    if (ini < 1) ini = 1;
    for (var i = ini; i <= fin; i++) {
      $scope.pCar.push({
        no: i
      });
    }

    if ($scope.cpCar >= $scope.pCar.length)
      $scope.cpCar = $scope.pCar.length - 1;
  };
  $scope.setPageCar = function(index) {
    $scope.cpCar = index - 1;
  };
  /*
  ███████ ██    ██ ███    ██  ██████ ██  ██████  ███    ██ ███████ ███████
  ██      ██    ██ ████   ██ ██      ██ ██    ██ ████   ██ ██      ██
  █████   ██    ██ ██ ██  ██ ██      ██ ██    ██ ██ ██  ██ █████   ███████
  ██      ██    ██ ██  ██ ██ ██      ██ ██    ██ ██  ██ ██ ██           ██
  ██       ██████  ██   ████  ██████ ██  ██████  ██   ████ ███████ ███████
  */
  $scope.declaraArea = function() {
    $scope.area = {
      id_area: 0,
      area: '',
      carrera: {}
    };
  };
  $interval(function() {
    $scope.obtieneCarreras();
    $scope.obtieneAreas();
  }, 10000);
  /*
   ██████  ██████  ████████ ███████ ███    ██ ███████ ██████
  ██    ██ ██   ██    ██    ██      ████   ██ ██      ██   ██
  ██    ██ ██████     ██    █████   ██ ██  ██ █████   ██████
  ██    ██ ██   ██    ██    ██      ██  ██ ██ ██      ██   ██
   ██████  ██████     ██    ███████ ██   ████ ███████ ██   ██
  */
  $scope.obtieneCarreras = function() {
    $http.get("./php/dbCarreras.php?action=getCarreras")
      .success(function(data) {
        $scope.carreras = data;
      });
  };
  $scope.obtieneAreas = function() {
    $http.get("./php/dbAreas.php?action=getAreas")
      .success(function(data) {
        $scope.areas = data;
        $scope.configPagesCar();
      });
  };
  /*
  ███████ ██████  ██ ████████  █████  ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  █████   ██   ██ ██    ██    ███████ ██████
  ██      ██   ██ ██    ██    ██   ██ ██   ██
  ███████ ██████  ██    ██    ██   ██ ██   ██
  */
  $scope.editarArea = function(p) {
    $scope.declaraArea();
    if (p) {
      console.log('editando: ', p);
      $scope.editandoArea = true;
      $scope.area = angular.copy(p);
      $scope.areaRespaldo = angular.copy($scope.area);
      console.log('carrera->', _.find($scope.carreras, ['id_carrera', p.id_carrera]));
      $scope.area.carrera = angular.copy(_.find($scope.carreras, ['id_carrera', p.id_carrera]));
      console.log('editando->', $scope.area);
    } else {
      $scope.editandoArea = false;
    }
  };
  /*
   ██████  ██    ██  █████  ██████  ██████   █████  ██████
  ██       ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
  ██   ███ ██    ██ ███████ ██████  ██   ██ ███████ ██████
  ██    ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
   ██████   ██████  ██   ██ ██   ██ ██████  ██   ██ ██   ██
  */
  $scope.guardarArea = function() {
    if ($scope.editandoArea) {
      $http.post('./php/dbAreas.php?action=setArea', {
        'area': $scope.area
      }).success(function(data) {
        var idx = _.findIndex($scope.areas, $scope.areaRespaldo);
        if (idx > -1) {
          if ($scope.area.carrera) {
            $scope.area.id_carrera = angular.copy($scope.area.carrera.id_carrera);
          }
          $scope.areas[idx] = $scope.area;
        }
        alertify.success('Area Modificada!');
      }).error(function(data) {});
    } else {
      $http.post('./php/dbAreas.php?action=setArea', {
        'area': $scope.area
      }).success(function(data) {
        $scope.area.id_area = data.success;
        if ($scope.area.carrera) {
          $scope.area.id_carrera = angular.copy($scope.area.carrera.id_carrera);
        }
        $scope.areas.push($scope.area);
        alertify.success('Area Guardada!');
      });
    }
    $('#modalArea').modal('hide');
  };
  /*
  ███████ ██      ██ ███    ███ ██ ███    ██  █████  ██████
  ██      ██      ██ ████  ████ ██ ████   ██ ██   ██ ██   ██
  █████   ██      ██ ██ ████ ██ ██ ██ ██  ██ ███████ ██████
  ██      ██      ██ ██  ██  ██ ██ ██  ██ ██ ██   ██ ██   ██
  ███████ ███████ ██ ██      ██ ██ ██   ████ ██   ██ ██   ██
  */
  $scope.eliminarArea = function(p) {
    $http.post("./php/dbAreas.php?action=dropArea", {
      'id_area': p.id_area
    }).success(function(data) {
      var idx = _.findIndex($scope.areas, p);
      if (idx > -1) {
        $scope.areas.splice(idx, 1);
        alertify.success('Area Eliminada!');
      }
    });
  };

});

function soloLetras(e) {
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
  especiales = [8, 37, 39, 46];

  tecla_especial = false
  for(var i in especiales) {
      if(key == especiales[i]) {
          tecla_especial = true;
          break;
      }
  }

  if(letras.indexOf(tecla) == -1 && !tecla_especial)
      return false;
};
