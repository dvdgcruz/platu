create table if not exists designaciones(
  id_formato int primary key auto_increment not null,
  formato varchar(30) not null,
  codigo varchar(30) not null,
  revision int not null,
  fechaExpedicion bigint not null,
  id_tutor int not null,
  id_carrera int not null,
  id_grupo int not null,
  periodo varchar(100) not null
)
create table if not exists entrevistaInicial(
  --ficha de identificacion--
  id_formato int primary key auto_increment not null,
  formato varchar(30) not null,
  codigo varchar(30) not null,
  revision int not null,
  id_alumno int not null,
  matricula varchar(15) not null,
  id_cuatrimestre int not null,
  fechaExpedicion bigint not null,
  edad int not null,
  estado_Civil varchar(20) not null,
  id_carrera int not null,
  id_grupo int not null,
  nss varchar(20) not null,
  lugar_Procedencia text not null,
  direccion_Actual text not null,
  telefono_Casa bigint not null,
  telefono_Celular bigint not null,
  nombre_responsable varchar(100) not null,
  telefono bigint not null,
  id_tutor int not null,
  --aspectos socioeconomicos
  2_1 text not null,
  2_2 text not null,
  2_3 text not null,
  2_3 text not null,
  2_4 text not null,
  2_5 text not null,
  2_6 text not null,
  sueldo float not null,
  hora int not null,
  2_7 text not null,
  2_8 text not null,
  2_9 text not null,
  2_10 text not null,
  2_11 text not null,
  2_12 text not null,
  2_13 text not null,
  2_14 text not null,
  --aspectos de salud--
  3_1 text not null,
  3_2 text not null,
  3_3 text not null,
  3_4 text not null,
  3_5 text not null,
  quien text not null,
  3_6 text not null,
  3_7 text not null,
  3_8 text not null,
  3_9 text not null,
  cantidad_Frecuencia text not null,
  obesidad tinyint null,
  bajo_de_peso tinyint null,
  falta_energia tinyint null,
  problemas_visuales tinyint null,
  problemas_auditivos tinyint null,
  discapacidades tinyint null,
  otro tinyint null,
  especifique text not null,
  --intereses personales--
  4_1 text not null,
  4_2 text not null,
  4_3 text not null,
  4_4 text not null,
  4_5 int not null,
  por_Que text not null,
  4_6 text not null,
  4_7 text not null,
  4_8 text not null,
  4_9 text not null,
  4_10 text not null,
  4_11 text not null,
  4_12 text not null,
  4_13 text not null,
  --aspectos academicos--
  5_1 text not null,
  especialidad text not null,
  fecha_egreso text not null,
  promedio float not null,
  5_2 tinyint not null,
  cual text null,
  5_3 text not null,
  cual2 text null,
  5_4 text not null,
  cual3 text null,
  5_5 text not null,
  cual4 text null,
  5_6 text not null,
  cual5 text null,
  5_7 text not null,
  5_8 text not null,
  5_9 text not null,
  esta_carrera_p_opcion tinyint null,
  5_10 text not null,
  5_11 text not null,
  5_12 text not null,
  5_13 int not null,
  5_14 int not null,
  5_15 tinyint not null,
  vulnerabilidad tinyint not null,
  aspectos_socioeconomicos tinyint null,
  aspecto_personal tinyint null,
  aspecto_academico tinyint null,
  observaciones_tutor text null
)
create table if not exists reporteIndividual(
  id_formato int primary key auto_increment not null,
  formato varchar(30) not null,
  codigo varchar(30) not null,
  revision int not null,
  fechaExpedicion bigint not null,
  id_alumno int not null,
  matricula varchar(15) not null,
  edad int not null,
  id_cuatrimestre int not null,
  lugar_Procedencia text not null,
  id_carrera int not null,
  municipio text not null,
  estado_residencia text not null,
  lugar_residencia text not null,
  telefono_tutorado bigint not null,
  nivel int not null,
  id_grupo int not null,
    ---datos familiares---
  nombre_padre text not null,
  vive tinyint null,
  edad
  ocupacion text not null,
  nomnre_madre text not null,
  vive tinyint null,
  edad int not null,
  ocupacion text not null,
  no_hijos
  ocupacion_padres text not null,
  direccion text not null,
  telefono text not null,
  personas_casa tinyint null,
  quien tinyint null,
  tienes_hermanos tinyint null,
  cuantos int null,
  grado_estudios_hermanos text null,
  10_ text not null,
  11_ text not null,
  12_ text not null,
  13_ text not null,
  14_ text not null,
  15_ text not null,
  16_ text not null,
  17_ text not null,
  18_ text not null,
  19_ text not null,
  20_ text not null,
  21_ text not null,
  --descripcion de la situacion actual--
  22_ text not null,
  23_ text not null,
  24_ text not null,
  25_ text not null,
  26_ text not null,
  27_ text not null,
  28_ text not null,
  29_ text not null,
  salud text not null,
  escolar text not null,
  comportamiento text not null,
  familia text not null,
  31_ text not null
)

    create table if not exists accionesgrupales(
      id_formato int primary key auto_increment not null,
      formato varchar(30) not null,
      codigo varchar(30) not null,
      revision int not null,
      fechaExpedicion date not null,
      id_tutor int not null,
      id_carrera int not null,
      id_grupo int not null,
      cuatrimestre varchar(100),
      horasParcial int not null,
      horasSemana int not null
    )
    create table if not exists tablaAccionesGrupales(
      id_formato int not null,
      fechaHoras varchar(50) not null,
      actividad text not null,
      resultados text not null,
      recursosMateriales text not null,
      recursosDidacticos text not null
    )
    create table if not exists programavisitas(
      id_formato int primary key auto_increment not null,
      formato varchar(30) not null,
      codigo varchar(30) not null,
      revision int not null,
      fechaExpedicion date not null,
      id_tutor int not null,
      id_carrera int not null,
      id_grupo int not null,
      numAlumnos int not null,
      estatus int default 1 not null,
    );
create table if not exists tablaprogramavisitas(
  id_formato int not null,
  numero int not null,
  fecha date not null,
  hora time not null,
  empresa text not null,
  objetivo text not null,
  asignatura text not null,
  responsable text not null
);
create table if not exists accionesIndividuales{
  id_formato int primary key auto_increment not null,
  formato varchar(30) not null,
  codigo varchar(30) not null,
  revision int not null,
  fechaExpedicion bigint not null,
  id_tutor int not null,
  id_carrera int not null,
  matricula varchar(15) not null,
  id_grupo int not null,
  id_cuatrimestre int not not null,
  id_alumno int not null not null,
--siatuacion actual del estudiante--
  descripcion_problematicas text not null,
  --acciones y estrategias de atencion al estudiante--
  canalizacion_area text not null,
  valores text not null,
  competencias text not null,
  metas text not null
)
create table if not exists tablaAccionesIndividuales(
  id_formato int not null,
  fechaHoras varchar(50) not null,
  actividad text not null,
  acuerdos text not null,
  recursosDidacticos text not null,
  responsableActividad text not null
)
create table if not exists informeParcialFinalGrupal(
  id_formato int primary key auto_increment not null,
  formato varchar(30) not null,
  codigo varchar(30) not null,
  revision int not null,
  fechaExpedicion bigint not null,
  id_tutor int not null,
  id_carrera int not null,
  id_grupo int not null,
  id_cuatrimestre varchar(100),
  id_parcial int not null
)
--1.- actividades realizadas por el grupo durante el mes:--
  create table if not exists tablaInformeParcialFinalGrupal1(
    id_formato int not null,
    fecha bigint not null,
    objetivo text not null,
    descripcion text not null,
    evidencia_fotografica MEDIUMBLOB
  )
  --2.- situacion academica parcial--
  create table if not exists tablaInformeParcialFinalGrupal2(
    id_formato int not null,
    id_alumno int not null,
    matricula varchar(15) not null,
    nombre_alumno varchar(100) not null,
    promedio float not null
  )
  --3.- Bajas o desercion--
  create table if not exists tablaInformeParcialFinalGrupal3(
    id_formato int not null,
    id_alumno int not null,
    matricula varchar(15) not null,
    nombre_alumno varchar(100) not null,
    motivo text not null
  )
  --3.- alumnos sobresalientes--
  create table if not exists tablaInformeParcialFinalGrupal4(
    id_formato int not null,
    id_alumno int not null,
    matricula varchar(15) not null,
    nombre_alumno varchar(100) not null,
    motivo text not null
  )
  create table if not exists informeAccionIndividual(
    id_formato int primary key auto_increment not null,
    formato varchar(30) not null,
    codigo varchar(30) not null,
    revision int not null,
    fechaExpedicion bigint not null,
    id_tutor int not null,
    id_carrera int not null,
    id_grupo int not null,
    id_cuatrimestre int not null,
    id_alumno int not null,
    matricula varchar(15)
  )
    --1.-rendimiento escolar--
    create table if not exists tablaInformeAccionIndividual1(
      fecha bigint not null,
      situacionActual text not null,
      motivo text not null,
      remedial text not null
    )
    --2.-asesorias y talleres--
    create table if not exists tablaInformeAccionIndividual2(
      fecha bigint not null,
      tema text not null,
      asesor text not null
    )
    --3.-graficas realizar una grafica entresituaciones relacion con remedial en programacion--
    --4.-asesorias y talleres--
    create table if not exists tablaInformeAccionIndividual3(
      actividad text not null,
      horario int not null,
      evidencia_fotografica MEDIUMBLOB
    )
    --4.-canalizacion y seguimiiento--
    create table if not exists tablaInformeAccionIndividual4(
      fecha bigint not null,
      area_apoyo text not null,
      motivo text not null,
    )
    create table if not exists entrevistaSeguimientoEstadias(
      id_formato int primary key auto_increment not null,
      formato varchar(30) not null,
      codigo varchar(30) not null,
      revision int not null,
      fechaExpedicion bigint not null,
      id_alumno int not null,

    )

--script de todos los formatos base(no se pudo implementar de esta maner)
create table if not exists formats{
  id_formato int primary key auto_increment,
  formato varchar(30),
  nombreFormato varchar(100),
  fechaExpedicion bigint,
  id_tutor int,
  id_carrera int,
  id_grupo int,
  cuatrimestre varchar(100),
  periodo varchar(100),
  matricula varchar(10),
  Edad int,
  Estado_Civil varchar(30),
  NSS varchar(30),
  Lugar_procedencia varchar(100),
  Direccion varchar(100),
  Telefono bigint,
  Telefono2 bigint,
  Telefono_casa bigint,
  Responsable bigint,
  Nombre_estudiante varchar(100),
  Tiempo_dedicado_tutorias int,
  Horas_semana int,
  Horas_parcial int,
  Tema_actividad_a_desarrollar varchar(200),
  Resultado varchar(200),
  Recursos_materiales varchar(200),
  Recursos_didacticos varchar(200),
  Proposito_accion_tutorial_objetivo varchar(200),
  parcial varchar(100),
  evidencia_fotografica MEDIUMBLOB,
  Promedio float,
  Asesor_acadamico varchar(100),
  Asesor_empresarial varchar(100),
  No. de alumnos int,
  Hora time,
  Empresa varchar(100),
  Asignatura_apoya_visita varchar(100),
  Profesor_responsable varchar(100),
  Docente_evaluador varchar(100)
  };
